DROP TABLE IF EXISTS Candidature;
DROP TABLE IF EXISTS Fiche_poste;
DROP TABLE IF EXISTS Offre;
DROP TABLE IF EXISTS Organisation;
DROP TABLE IF EXISTS Utilisateur;


CREATE TABLE Organisation (
    siren INTEGER UNIQUE NOT NULL,
    name VARCHAR(255) NOT NULL,
    org_type VARCHAR(255) NOT NULL,
    head_office VARCHAR(255) NOT NULL,
    PRIMARY KEY (siren)
);

CREATE TABLE Utilisateur (
    user_id INTEGER UNIQUE NOT NULL,
    PRIMARY KEY (user_id),
    mail VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    nom VARCHAR(255) NOT NULL,
    prenom VARCHAR(255) NOT NULL,
    phone INTEGER NOT NULL,
    datecrea DATE NOT NULL,
    statut VARCHAR(255) NOT NULL CHECK (statut IN ('actif', 'inactif')),
    user_type VARCHAR(255) NOT NULL CHECK (user_type IN ('administrateur', 'candidat', 'recruteur')),
    organisation INTEGER REFERENCES Organisation(siren)
);

CREATE TABLE Offre (
    offre_id INTEGER UNIQUE NOT NULL,
    state VARCHAR(255) NOT NULL CHECK (state IN ('non_publiee', 'publiee', 'expiree')),
    organisation INTEGER NOT NULL REFERENCES Organisation(siren),
    recruteur INTEGER NOT NULL REFERENCES Utilisateur(user_id),
    PRIMARY KEY (offre_id)
);

CREATE TABLE Candidature (
    id INTEGER UNIQUE NOT NULL Auto_Increment,
     PRIMARY KEY (id),
    candidat INTEGER NOT NULL,
    offre INTEGER NOT NULL,
    date DATE NOT NULL,
    FOREIGN KEY (candidat) REFERENCES Utilisateur(user_id),
    FOREIGN KEY (offre) REFERENCES Offre(offre_id)
);

CREATE TABLE Document (
    id INTEGER UNIQUE NOT NULL AUTO_INCREMENT ,
    candidature INTEGER NOT NULL,
    chemin VARCHAR(255) NOT NULL,
    type VARCHAR(255) NOT NULL CHECK (state IN ('cv', 'lm', 'cni')),
    FOREIGN KEY (candidature) REFERENCES Candidature(candidatureId),
);

CREATE TABLE Organisation (
    siren INTEGER UNIQUE NOT NULL,
    name VARCHAR(255) NOT NULL,
    org_type VARCHAR(255) NOT NULL,
    head_office VARCHAR(255) NOT NULL,
    PRIMARY KEY (siren)
);

CREATE TABLE Fiche_poste (
    poste_id INTEGER NOT NULL AUTO_INCREMENT,
    intitule VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    status VARCHAR(255) NOT NULL,
    resp VARCHAR(255) NOT NULL,
    type_metier VARCHAR(255) NOT NULL,
    loc VARCHAR(255) NOT NULL,
    horaire VARCHAR(255) NOT NULL,
    salairemin INTEGER NOT NULL,
    salairemax INTEGER NOT NULL,
    day_off INTEGER NOT NULL,
    remote BOOLEAN NOT NULL,
    offre_id INTEGER NOT NULL REFERENCES Offre(offre_id),
    PRIMARY KEY (poste_id)
);

CREATE TABLE Offre (
    offre_id INTEGER UNIQUE NOT NULL AUTO_INCREMENT,
    state VARCHAR(255) NOT NULL CHECK (state IN ('non_publiee', 'publiee', 'expiree')),
    organisation INTEGER NOT NULL REFERENCES Organisation(siren),
    date_v_fin Date NOT NULL ,
    date_v_debut Date NOT NULL ,
    PRIMARY KEY (offre_id)
);
