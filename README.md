# SR10_P2023_Mondesert_Pichon

# Projet SR10 - Application Web de Recrutement

## Cahier des charges

### 1. Besoin

Le projet vise à créer une plateforme de recrutement en ligne, facilitant la connexion entre entreprises en quête de talents et individus cherchant des opportunités professionnelles. Les fonctionnalités clés comprennent :

- Ajout d'organisations (entreprises, associations, etc.).
- Gestion complète des offres d'emploi.
- Affichage et recherche avancée d'offres.
- Possibilité pour les candidats de postuler en ligne.
- Suivi des candidatures et téléchargement des dossiers.

### 2. Description fonctionnelle

**Acteurs principaux :**

- Utilisateur : administrateur, recruteur, candidat.
- Organisation : entité cherchant à recruter.
- Offre d'emploi : détaillée avec diverses informations.
- Candidature : enregistre la date et les documents du dossier.

### 3. Enchaînement des écrans

- Page d'authentification/inscription.
- Écran candidat avec options spécifiques.
- Écrans administrateur et recruteur accessibles via des boutons correspondants.

#### Écrans administrateur :

- Gestion des utilisateurs.
- Gestion des organisations.

#### Écrans recruteur :

- Gestion des offres.
- Gestion des recruteurs.
- Gestion des candidatures.

#### Écrans candidat :

- Visualisation des offres d'emploi avec options de tri et filtres.
- Possibilité de postuler à une offre.

## Livrables

- Cas d'utilisation : [Diagramme](\TD1_Conception\CasdUtilisation.png)
- Conception : [UML](\TD1_Conception\UML.vpd.pdf), [Modèle BDD](\TD1_Conception\DBCrea2.sql)

## Test du code

Des tests approfondis ont été réalisés sur les modèles et les routes essentielles de l'application. Une batterie de tests garantit la fonctionnalité, la robustesse et la performance. Pour les tests de recherche, consultez : [\TD3\SR10_JOB\model.test.js](\TD3\SR10_JOB\model.test.js)


_mot de passe tuxa : *********_
