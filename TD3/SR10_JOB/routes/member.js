var express = require('express');
var router = express.Router();
var userModel = require("../models/Utilisateur");
var candidModel = require("../models/Candidature");
var documentModel = require("../models/Document");
var offreModel = require("../models/Offre");
var memberProfile = require('./memberProfile');
var common = require('./common');
var crypto = require('crypto');
var base64url = require('base64url');

var multer = require('multer');

var my_storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'user_documents');
  },
  filename: function (req, file, cb) {
    let my_extension = file.originalname.slice(file.originalname.lastIndexOf("."));
    let fileType = '';

    if (file.fieldname === 'CvFileInput') {
      fileType = 'cv';
    } else if (file.fieldname === 'LmFileInput') {
      fileType = 'lm';
    } else if (file.fieldname === 'CniFileInput') {
      fileType = 'cni';
    }
    const candidatureId = req.params.id;
    const userId = req.session.userId;
    const userName = req.session.userName;

    // Encode candidatureId and userId using Base64
    const encodedCandidatureId = base64url.encode(candidatureId.toString());
    const encodedUserId = base64url.encode(userId.toString());

    const fileName = `${userName}-${encodedCandidatureId}-${encodedUserId}-${fileType}${my_extension}`;
    cb(null, fileName);
  }
});

var upload_documents = multer({ storage: my_storage });

router.get('/home', function (req, res, next) {
  const userId = req.session.userId;
  const resultsPerPage = 5;
  if (!userId) {
    return res.redirect('/');
  }
  if (req.session.user_type !== 'candidat') {
    return res.redirect('/');
  }
  result = offreModel.getNotAppliedByUserOffer(userId, resultsPerPage, function (error, result) {
    res.render('Member/HomePage', { title: 'Homepage - member', offres: result, totalPages: 1 });
  });
});

router.get('/view-offre/:id', function (req, res, next) {
  const userId = req.session.userId;

  const id = req.params.id;
  if (!userId) {
    return res.redirect('/');
  }
  if (req.session.user_type !== 'candidat') {
    return res.redirect('/');
  }
  result = offreModel.getById(id, function (error, result) {
    res.render('Member/FullOffer', { title: 'Homepage - offre', data: result[0] });
  });
});

//#region Candidature
router.get('/candidature-offre/:id', function (req, res, next) {
  const id = req.params.id;

  const userId = req.session.userId;
  if (!userId) {
    return res.redirect('/');
  }
  if (req.session.user_type !== 'candidat') {
    return res.redirect('/');
  }

  if (req.session.uploaded_files == undefined) {
    console.log('Init uploaded files array');
    req.session.uploaded_files = [];
  }

  result = offreModel.getById(id, function (error, result) {
    console.log(result);
    res.render('Member/CandiOffer', { title: 'Homepage - offre', userId: userId, data: result[0], files_array: req.session.uploaded_files });
  });
});

router.post('/candidater/:id', function (req, res) {
  const userId = req.session.userId;
  if (!userId) {
    return res.redirect('/');
  }
  if (req.session.user_type !== 'candidat') {
    return res.redirect('/');
  }

  upload_documents.fields([
    { name: 'CvFileInput', maxCount: 1 },
    { name: 'LmFileInput', maxCount: 1 },
    { name: 'CniFileInput', maxCount: 1 }
  ])(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      // Handle multer errors
      res.status(500).send('An error occurred while uploading the files.');
    } else if (err) {
      // Handle other errors, if any
      res.status(500).send('An error occurred while processing the request.');
    } else {
      const offreId = req.params.id;
      const cvFile = req.files['CvFileInput'][0];
      const lmFile = req.files['LmFileInput'] ? req.files['LmFileInput'][0] : null;
      const cniFile = req.files['CniFileInput'] ? req.files['CniFileInput'][0] : null;

      // Prepare the candidature data
      const candidatureData = {
        candidat: userId,
        offre: offreId,
        date: new Date().toISOString().slice(0, 10),
        files: {
          cv: {
            nom: cvFile.filename,
            chemin: cvFile.path,
            type: 'cv'
          },
          lm: lmFile ? {
            nom: lmFile.filename,
            chemin: lmFile.path,
            type: 'lm'
          } : null,
          cni: cniFile ? {
            nom: cniFile.filename,
            chemin: cniFile.path,
            type: 'cni'
          } : null
        }
      };
      console.log(candidatureData);

      candidModel.create(candidatureData, (error, candidatureId, docs) => {
        if (error) {
          req.flash('error', 'Une erreur a eu lieu');
        } else {
          console.log(docs);
          req.flash('success', `Vous avez candidate avec succes (${docs} documents ont été ajoutés)`);
        }
        res.redirect(`/member/view-offre/${offreId}`);
      });
    }
  });
});

router.post('/candidater-addfile/:id', function (req, res) {
  const userId = req.session.userId;
  if (!userId) {
    return res.redirect('/');
  }
  if (req.session.user_type !== 'candidat') {
    return res.redirect('/');
  }

  upload_documents.fields([
    { name: 'CvFileInput', maxCount: 1 },
    { name: 'LmFileInput', maxCount: 1 },
    { name: 'CniFileInput', maxCount: 1 }
  ])(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      // Handle multer errors
      res.status(500).send('An error occurred while uploading the files.');
    } else if (err) {
      // Handle other errors, if any
      res.status(500).send('An error occurred while processing the request.');
    } else {
      const candiId = req.params.id;
      const cvFile = req.files['CvFileInput'] ? req.files['CvFileInput'][0] : null;
      const lmFile = req.files['LmFileInput'] ? req.files['LmFileInput'][0] : null;
      const cniFile = req.files['CniFileInput'] ? req.files['CniFileInput'][0] : null;
      let files = []
      // Prepare the candidature data
      if (cvFile) {
        cv = {
          candidature: candiId,
          nom: cvFile.filename,
          chemin: cvFile.path,
          type: 'cv'
        };
        files.push(cv);
      }
      if (lmFile) {
        lm = {
          candidature: candiId,
          nom: lmFile.filename,
          chemin: lmFile.path,
          type: 'lm'
        };
        files.push(lm);
      }
      if (cniFile) {
        cni = {
          candidature: candiId,
          nom: cniFile.filename,
          chemin: cniFile.path,
          type: 'cni'
        };
        files.push(cni);
      }

      console.log(files);

      documentModel.createMultiple(files, (error, candidatureId, docs) => {
        if (error) {
          req.flash('error', 'Une erreur a eu lieu');
        } else {
          console.log(docs);
          req.flash('success', `Vous avez candidate avec succes (${docs} documents ont été ajoutés)`);
        }
        res.redirect(`/member/profil/candid/edit/${candiId}`);
      });
    }
  });
});
//#endregion
router.post('/search-offer', function (req, res) {
  const userId = req.session.userId;
  if (!userId) {
    return res.redirect('/');
  }
  if (req.session.user_type !== 'candidat') {
    return res.redirect('/');
  }

  const currentPage = req.body.page || 1; // Default to the first page if not specified
  const resultsPerPage = 5; // Specify the desired number of results per page
  const offset = (currentPage - 1) * resultsPerPage;
  const filters = {
    searchfield: req.body.searchfield || '',
    salaire: req.body.salaire,
    head_office: req.body.head_office,
    remote: req.body.remote,
    dayoff: req.body.dayoff,
    org: req.body.org,
  };
  offreModel.searchFilteredOffers(filters, userId, resultsPerPage, offset, function (error, results) {
    if (error) {
      console.error('An error occurred during the search:', error);
      return res.render('Member/HomePage', {
        offres: [],
        currentPage: currentPage,
        totalPages: 1,
        error: error
      });
    }
    offreModel.countFilteredOffers(filters, userId, function (error, totalCount) {
      if (error) {
        console.error('An error occurred while fetching the total count:', error);
        return res.render('Member/HomePage', {
          offres: [],
          currentPage: currentPage,
          totalPages: 1,
          error: error
        });
      }
      console.log(totalCount);
      console.log(results);
      const totalPages = Math.ceil(totalCount / resultsPerPage);
      console.log(totalPages);

      res.render('Member/HomePage', {
        offres: results,
        currentPage: currentPage,
        totalPages: totalPages,
        criteria: filters
      });
    });
  });
});

router.use('/profil', memberProfile);
router.use('/common', common);

module.exports = router;