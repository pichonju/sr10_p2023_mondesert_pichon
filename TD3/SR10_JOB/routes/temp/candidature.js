var express = require('express');
var router = express.Router();

var candidatureModel = require("../../models/Candidature")

// Route pour afficher toutes les candidatures
router.get('/listecandidature', (req, res, next) => {

  result = candidatureModel.getAll(function (error, result) {
    if (error) {
      // Rendre la vue d'erreur avec le message d'erreur
      return res.render('erreur', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
    }

    // Rendre la vue avec les détails de l'utilisateur
    res.render('ListeAllCandidature', { title: 'Candidature', candidatures: result });
  });
});

// Route pour afficher les détails d'une candidature par id candidat
router.get('/candidature/:id', (req, res, next) => {
  const id = req.params.id;

  result = userModel.getByCandidatId(id, function (error, result) {
    if (error) {
      // Rendre la vue d'erreur avec le message d'erreur
      return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
    }

    // Rendre la vue avec les détails de la candidature
    res.render('CandidatureByIdCandidat', { title: 'Candidature', data: result });
  });
});

// Route pour afficher les détails d'une candidature par id Offre
router.get('/candidature/:id', (req, res, next) => {
  const id = req.params.id;

  result = userModel.getByOffreId(id, function (error, result) {
    if (error) {
      // Rendre la vue d'erreur avec le message d'erreur
      return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
    }

    // Rendre la vue avec les détails de la candidature
    res.render('CandidatureByIdOffre', { title: 'Candidature', data: result });
  });
});


router.post('/nvCandidature', function (req, res, next) {
  //récupérer les données passées via le body de la requête post :

  const Nouvel_Candidature = {
    candidat: req.body.candidat,
    offre: req.body.offre,
    date: req.body.date,
    pieces_dossier: req.body.pieces_dossier
  };

  //utiliser le model pour enregistrer les données récupérées dans la BD
  result = candidatureModel.create(Nouvel_Candidature, function (error, result) {
    if (error) {
      // Rendre la vue d'erreur avec le message d'erreur
      return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
    }
    // Dire si crée avec succes
    return res.status(200).send('Utilisateur créé avec succès');
  });
});

module.exports = router;