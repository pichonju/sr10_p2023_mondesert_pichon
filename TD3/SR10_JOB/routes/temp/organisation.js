var express = require('express');
var router = express.Router();

var organisationModel = require("../../models/Organisation")


//route pour afficher la liste de toutes les orga
router.get('/listeorganisation', function (req, res, next) {
    result = organisationModel.getAll(function (error, result) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('erreur', { message: 'Une erreur s\'est produite lors de la récupération des utilisateurs.' });
        }
         // Rendre la vue avec la liste des orgas
        res.render('ListeAllOrga', { title: 'List des orgas', orga: result });
    });
});

// Route pour afficher les orgas par Id
router.get('/id/:id', (req, res, next) => {
    const id = req.params.id;

    result = organisationModel.getById(id, function (error, result) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }

        // Rendre la vue avec une orga par Id
        res.render('ListeOrgaId', { title: 'Orga', orga: result });
    });
});

// Route pour afficher les orgas par nom
router.get('/nom/:nom', (req, res, next) => {
    const nom = req.params.nom;

    result = organisationModel.getByNom(nom, function (error, result) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }

        // Rendre la vue avec une orga par nom
        res.render('ListeOrgaNom', { title: 'Orga', orga: result });
    });
});

// Route pour afficher les orgas par type
router.get('/type/:org_type', (req, res, next) => {
    const org_type = req.params.org_type;

    result = organisationModel.getByType(org_type, function (error, result) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }

        // Rendre la vue avec les orgas par type
        res.render('ListeOrgaType', { title: 'Orga', orga: result });
    });
});

// Route pour afficher les orgas par localisation
router.get('/location/:head_office', (req, res, next) => {
    const head_office = req.params.head_office;

    result = organisationModel.getByLocation(head_office, function (error, result) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }

        // Rendre la vue avec les orgas par localisation
        res.render('ListeOrgaLocation', { title: 'Orga', orga: result });
    });
});

router.post('/nvOrganisation', function (req, res, next) {
    //récupérer les données passées via le body de la requête post :
    
    const Nouvel_Organisation = {
      siren : req.body.siren,
      name : req.body.name,
      org_type : req.body.org_type,
      head_office : req.body.head_office
    };
    
    //utiliser le model pour enregistrer les données récupérées dans la BD
    result = organisationModel.create(Nouvel_Organisation, function (error, result) {
      if (error) {
        // Rendre la vue d'erreur avec le message d'erreur
        return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
      }
      // Dire si crée avec succes
      return res.status(200).send('Utilisateur créé avec succès');
    });
    });

module.exports = router;