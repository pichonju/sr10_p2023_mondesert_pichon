var express = require('express');
var router = express.Router();
var offreModel = require("../../models/Offre")

// Route pour afficher les offre pas état
router.get('/state/:state', (req, res, next) => {
    const state = req.params.state;
  
    result = offreModel.getByState(state, function (error, result) {
      if (error) {
        // Rendre la vue d'erreur avec le message d'erreur
        return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
      }

      // Rendre la vue avec les détails de l'utilisateur

      res.render('ListeOffreState', { title: 'Offre', offres: result });

    });
  });

  // Route pour afficher les offres par organisation
router.get('/organisation/:organisation', (req, res, next) => {
  const organisation = req.params.organisation;

  result = offreModel.getByOrganisation(organisation, function (error, result) {
    if (error) {
      // Rendre la vue d'erreur avec le message d'erreur
      return res.render('erreur', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
    }

    // Rendre la vue avec les détails de l'utilisateur
    res.render('ListeOffreOrga', { title: 'Offre', offres: result });
  });
});

  // Route pour afficher les offres par Id
  router.get('/id/:id', (req, res, next) => {
    const id = req.params.id;
  
    result = offreModel.getById(id, function (error, result) {
      if (error) {
        // Rendre la vue d'erreur avec le message d'erreur
        return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
      }
  
      // Rendre la vue avec les détails de l'utilisateur
      res.render('ListeOffreId', { title: 'Offre', offres: result });
    });
  });



//route pour afficher la liste de toutes les offres
router.get('/listeoffre', function (req, res, next) {
  result = offreModel.getAll(function (error, result) {
      if (error) {
          // Rendre la vue d'erreur avec le message d'erreur
          return res.render('erreur', { message: 'Une erreur s\'est produite lors de la récupération des utilisateurs.' });
      }
      res.render('ListeAllOffre', { title: 'List des offres', offres: result });
  });
});

router.post('/nvOffre', function (req, res, next) {
  //récupérer les données passées via le body de la requête post :
  
  const Nouvel_Offre = {
    state : req.body.state,
    organisation : req.body.organisation,
    recruteur : req.body.recruteur
  };
  
  //utiliser le model pour enregistrer les données récupérées dans la BD
  result = offreModel.create(Nouvel_Offre, function (error, result) {
    if (error) {
      // Rendre la vue d'erreur avec le message d'erreur
      return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
    }
    // Dire si crée avec succes
    return res.status(200).send('Utilisateur créé avec succès');
  });
  });
  

  module.exports = router;