var express = require('express');
var router = express.Router();
var userModel = require("../../models/Utilisateur")


//route pour afficher la liste de tous les utilisateurs
router.get('/listeutilisateurs', function (req, res, next) {
  result = userModel.getAll(function (error, result) {
    if (error) {
      // Rendre la vue d'erreur avec le message d'erreur
      return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des utilisateurs.', error: error });
    }
    res.render('ListeUtilisateurs', { title: 'List des utilisateurs', users: result });
  });
});

// Route pour afficher les détails d'un utilisateur spécifique
router.get('/utilisateurs/:id', (req, res, next) => {

    const id = req.params.id;
  
    result = userModel.getById(id, function (error, result) {
      if (error) {
        // Rendre la vue d'erreur avec le message d'erreur
        return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
      }
  
      // Rendre la vue avec les détails de l'utilisateur
      res.render('Member/MyInfo', { title: 'Utilisateur', data: result });
    });
  });


//initialisation
router.use(express.urlencoded({ extended: true }));
router.use(express.json());


router.post('/nvUser', function (req, res, next) {
  /*récupérer les données passées via le body de la requête post :
  Exemple :
   const user_fname = req.body.fname;
   const user_lname = req.body.lname; 
  */
  const Nouvel_Utilisateur = {
    user_id: '5',
    mail: req.body.email,
    password: req.body.password,
    nom: req.body.nom,
    prenom: req.body.prenom,
    phone: req.body.phone,
    datecrea: new Date(),
    statut: 'actif',
    user_type: 'candidat',
    organisation: null
  };

  //utiliser le model pour enregistrer les données récupérées dans la BD
  result = userModel.create(Nouvel_Utilisateur, function (error, result) {
    if (error) {
      // Rendre la vue d'erreur avec le message d'erreur
      return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
    }
    // Dire si crée avec succes
    return res.status(200).send('Utilisateur créé avec succès');
  });
});

router.post('/updateUser', function (req, res, next) {
  //récupérer les données passées via le body de la requête post :

  const Utilisateur = {
    mail: req.body.email,
    password: req.body.password,
    nom: req.body.nom,
    prenom: req.body.prenom,
    phone: req.body.phone,
    datecrea: new Date(),
    statut: 'actif',
    user_type: 'candidat',
    organisation: req.body.organisation
  };
  const id = req.body.user_id;

  //utiliser le model pour enregistrer les données récupérées dans la BD
  result = userModel.updateById(id, Utilisateur, function (error, result) {
    if (error) {
      // Rendre la vue d'erreur avec le message d'erreur
      return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
    }
    // Dire si crée avec succes
    return res.status(200).send('Utilisateur créé avec succès');
  });
});

router.post('/deleteUser', function (req, res, next) {
  //récupérer les données passées via le body de la requête post :
  const id = req.body.user_id;

  //utiliser le model pour enregistrer les données récupérées dans la BD
  result = userModel.deleteById(id, function (error, result) {
    if (error) {
      // Rendre la vue d'erreur avec le message d'erreur
      return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
    }
    // Dire si crée avec succes
    return res.status(200).send('Utilisateur créé avec succès');
  });
});


module.exports = router;
