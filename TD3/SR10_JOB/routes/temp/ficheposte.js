var express = require('express');
var router = express.Router();
var ficheModel = require("../../models/FichePoste")

router.get('/listefichepostes', function (req, res, next) {
    result = ficheModel.getAll(function (error, result) {
        res.render('ListeFichePoste', { title: 'List des Fiches de poste', fiches: result });
    });
});

router.post('/nvFichePoste', function (req, res, next) {
    //récupérer les données passées via le body de la requête post :
  
    const Nouvel_Fiche = {
      poste_id : req.body.poste_id,
      intitule : req.body.intitule,
      description : req.body.description,
      status : req.body.pieces_status,
      resp : req.body.resp,
      type_metier : req.body.type_metier,
      loc : req.body.loc,
      horaire : req.body.horaire,
      salairemin : req.body.salairemin,
      salairemax : req.body.salairemax,
      day_off : req.body.day_off,
      remote : req.body.remote,
      offre_id : req.body.offre_id
    
    };


  
    //utiliser le model pour enregistrer les données récupérées dans la BD
    result = ficheModel.createFichePoste(Nouvel_Fiche, function (error, result) {
      if (error) {
        // Rendre la vue d'erreur avec le message d'erreur
        return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
      }
      // Dire si crée avec succes
      return res.status(200).send('Utilisateur créé avec succès');
    });
  });

module.exports = router;
