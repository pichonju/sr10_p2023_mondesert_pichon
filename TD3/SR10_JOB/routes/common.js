var express = require('express');
var router = express.Router();

router.get('/getfile', function(req, res, next) {
    try {
      res.download('./user_documents/'+req.query.fichier_cible);
    } catch (error) {
      res.send('Une erreur est survenue lors du téléchargement de '+req.query.fichier_cible+' : '+error);
    }
  });

  module.exports = router;
