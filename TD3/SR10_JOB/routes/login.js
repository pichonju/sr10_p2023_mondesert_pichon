const mysql = require('mysql');
const express = require('express');
const session = require('express-session');
const router = express.Router();
const validator = require('validator');
const userModel = require('../models/Utilisateur');
const pass = require('../public/javascripts/pass.js')




// http://localhost:3000/
router.get('/', (req, res) => {
    res.render('Connexion');
});

router.post('/login', function (req, res, next) {
    const user_email = req.body.email;
    const user_password = req.body.password;
    const maxFailedAttempts = 5;
    //const blockDuration = 10 * 60 * 1000;
    const blockDuration = 30 * 1000; // 30 seconds

    const blockedUntil = req.session.blockedUntil;

    if (blockedUntil && blockedUntil > Date.now()) {
        const remainingTime = Math.ceil((blockedUntil - Date.now()) / 1000);
        return res.render('Connexion', { error: `Too many failed login attempts Please try again after ${remainingTime} sec.` });
    } else if (blockedUntil && blockedUntil < Date.now()) {
        req.session.failedAttempts = 0;
    }

    if (validator.isEmpty(user_email) || validator.isEmpty(user_password)) {
        res.render('Connexion', { error: 'Veuillez remplir tous les champs' });
        return;
    }

    if (!validator.isEmail(user_email)) {
        res.render('Connexion', { error: 'Veuillez entrer un email valide' });
        return;
    }

    if (user_email && user_password) {
        userModel.getByEmail(user_email, function (error, results) {
            if (error) {
                throw error;
            }

            if (results.length > 0) {
                const user = results[0];
                const hashed_password = user.password;
                pass.comparePassword(user_password, hashed_password, function (isMatch) {
                    console.log(user_password);
                    console.log(isMatch);
                    if (isMatch) {
                        if (user.statut === 'actif') {
                            req.session.userId = user.user_id;
                            req.session.userName = user.nom;
                            req.session.userSurname = user.prenom;
                            req.session.user_type = user.user_type;

                            req.session.failedAttempts = 0;
                            req.session.blockedUntil = undefined;

                            if (user.user_type === 'administrateur') {
                                res.redirect('/admin/home');
                            } else if (user.user_type === 'candidat') {
                                res.redirect('/member/home');
                            } else if (user.user_type === 'recruteur') {
                                req.session.organisation = user.organisation;
                                res.redirect('/recruiter/home');
                            } else {
                                res.send('Unknown user type');
                            }
                        } else {
                            res.render('Connexion', { error: 'Account is inactive' });
                        }
                    } else {
                        req.session.failedAttempts = (req.session.failedAttempts || 0) + 1;
                        if (req.session.failedAttempts >= maxFailedAttempts) {
                            req.session.blockedUntil = Date.now() + blockDuration;
                            return res.render('Connexion', { error: `Too many failed login attempts. Please try again after ${Math.ceil(blockDuration / 1000)} sec.` });
                        } else {
                            res.render('Connexion', {
                                error: 'Incorrect Username and/or Password!',
                            });
                        }
                    }
                });
            } else {
                res.render('Connexion', { error: 'Please enter Username and Password!' });
            }
        });
    };
});
router.get('/signinpage', (req, res) => {
    res.render('CreerCompte', { data: {} });
});

router.post('/signin', function (req, res, next) {
    const data = { nom, prenom, email, telephone, password, confirmation } = req.body;

    const date = new Date();
    const formattedDate = date.toISOString().slice(0, 10);

    if (!validator.isEmail(email)) {
        res.render('CreerCompte', { error: 'Email non valide', data: data });
        return;
    }
    // On vérifie que les champs ne sont pas vides
    if (validator.isEmpty(email) || validator.isEmpty(password) || validator.isEmpty(nom) || validator.isEmpty(telephone) || validator.isEmpty(prenom)) {
        res.render('CreerCompte', { error: 'Veuillez remplir tous les champs', data: data });
        return;
    }
    if (!validator.isMobilePhone(telephone, "fr-FR")) {
        res.render('CreerCompte', { error: 'Veuillez entrer un numéro de téléphone valide', data: data });
        return;
    }

    // Validation des données du mot de passe
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

    if (!passwordRegex.test(password)) {
        res.render('CreerCompte', { error: 'Le mot doit contenir 8 caractères dont au moins une majuscule, une minuscule, un chiffre, un caractère spécial', data: data });
        return;
    }
    if (password !== confirmation) {
        res.render('CreerCompte', { error: 'Les mots de passe ne correspondent pas', data: data });
        return;
    }


    userModel.getByEmail(email, function (error, result_read_model) {
        if (error) {
            res.render('CreerCompte', { error: 'An error occurred during user creation', data: data });
            return;
        }

        if (result_read_model.length !== 0) {
            res.render('CreerCompte', { error: 'Email déjà existant' });
            return;
        }

        pass.generateHash(password, function (hash) {


            // Création de l'utilisateur
            const utilisateur = {
                mail: email,
                password: hash,
                nom: nom,
                prenom: prenom,
                phone: telephone,
                datecrea: formattedDate,
                statut: "inactif",
                user_type: "candidat",
                organisation: null
            };


            userModel.create(utilisateur, function (error, result_user_create) {
                if (error) {
                    res.render('CreerCompte', { error: 'An error occurred during user creation', data: data });
                } else {
                    res.render('Welcome');
                }
            });
        });
    });
});

router.get('/logout', function (req, res) {
    // Déconnecter l'utilisateur en supprimant la session
    req.session.destroy(function (error) {
        if (error) {
            // Gérer l'erreur de déconnexion
            console.error('Erreur lors de la déconnexion :', error);
        }
        // Rediriger vers la page d'accueil ou toute autre page après la déconnexion
        res.redirect('/');
    });
});

module.exports = router;
