var express = require('express');
var router = express.Router();
var userModel = require("../models/Utilisateur")
var offreModel = require("../models/Offre")
var ficheModel = require("../models/FichePoste")
var candidModel = require("../models/Candidature")
var recruiterProfile = require("./recruiterProfile")
const validator = require('validator');
var common = require('./common');


router.get('/home', function (req, res, next) {
  if (!req.session.userId) {
    return res.redirect('/');
  }
  if (req.session.user_type !== 'recruteur') {
    return res.redirect('/');
  }
  if (req.session.user_type !== 'recruteur') {
    return res.redirect('/');
  }
  const orga = req.params.orga;
  result = offreModel.getByOrganisation(req.session.organisation, function (error, result) {
    res.render('Recruteur/RecruiterHomePage', { title: 'Homepage - recruteur', offre: result });
  });
});



// Route pour creer une offre
router.get('/createOffer', (req, res, next) => {
  const userId = req.session.userId;
  const orga = req.session.organisation;
  if (!userId) {
    // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
    return res.redirect('/');
  }
  if (req.session.user_type !== 'recruteur') {
    return res.redirect('/');
  }
  result = userModel.getById(userId, function (error, result) {
    if (error) {
      // Rendre la vue d'erreur avec le message d'erreur
      return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
    }
    // Rendre la vue avec les détails de l'offre
    res.render('Recruteur/CreateOffer', { title: 'Utilisateur', data: result });
  });
});

router.post('/creatingOffer', (req, res) => {
  const userId = req.session.userId;
  const offreData = {
    state: req.body.statut,
    organisation: req.session.organisation,
    date_v_debut: req.body.date_v_debut,
    date_v_fin: req.body.date_v_fin,
    state: req.body.statut
  };
  const fichePosteData = {
    intitule: req.body.intitule,
    description: req.body.description,
    type_metier: req.body.type_metier,
    resp: req.body.resp,
    loc: req.body.loc,
    horaire: req.body.horaire,
    salairemin: parseInt(req.body.salairemin),
    salairemax: parseInt(req.body.salairemax),
    day_off: parseInt(req.body.day_off),
    remote: parseInt(req.body.remote),

  };

  offreModel.createWithFiche(offreData, fichePosteData, (err, result) => {
    if (err) {
      console.error(err);
      return res.render('Recruteur/CreateOffer', { data: result, error: err });
    } else {
      return res.redirect('/recruiter/home');
    }
  });
});



router.get('/view-offre/:id', function (req, res, next) {
  const id = req.params.id;
  if (!req.session.userId) {
    return res.redirect('/');
  }
  if (req.session.user_type !== 'recruteur') {
    return res.redirect('/');
  }
  result = offreModel.getById(id, function (error, result) {
    res.render('Recruteur/FullOffer', { title: 'Homepage - offre', data: result[0] });
  });
});

// Route pour editer les détails d'une offre'
router.get('/editOffre/:id', (req, res, next) => {
  const id = req.params.id;
  const userId = req.session.userId;
  const orga = req.session.organisation;
  if (!userId) {
    // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
    return res.redirect('/');
  }
  if (req.session.user_type !== 'recruteur') {
    return res.redirect('/');
  }
  result = offreModel.getById(id, function (error, result) {
    if (error) {
      // Rendre la vue d'erreur avec le message d'erreur
      return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
    }
    // Rendre la vue avec les détails de l'offre
    res.render('Recruteur/EditOffer', { title: 'Utilisateur', data: result[0] });
  });
});

router.post('/editingOffer/:id', function (req, res, next) {
  const Fiche = {
    intitule: req.body.intitule,
    description: req.body.description,
    resp: req.body.resp,
    type_metier: req.body.type_metier,
    loc: req.body.loc,
    horaire: req.body.horaire,
    salairemin: parseInt(req.body.salairemin),
    salairemax: parseInt(req.body.salairemax),
    day_off: parseInt(req.body.day_off),
    remote: parseInt(req.body.remote)
  };
  const Offre = {
    state: req.body.statut,
    date_v_debut: req.body.date_v_debut,
    date_v_fin: req.body.date_v_fin
  }

  const id = req.params.id;
  offreModel.updateById(id, Offre, function (error, updateResult) {
    if (error) {
      // Rendre la vue d'erreur avec le message d'erreur
      return res.render('Recruteur/EditOffer', { data: Offre, error: error });// 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
    }

  });
  //utiliser le model pour enregistrer les données récupérées dans la BD
  ficheModel.updateById(id, Fiche, function (error, updateResult) {
    if (error) {
      // Rendre la vue d'erreur avec le message d'erreur
      return res.render('Recruteur/EditOffer', { data: Fiche, error: error });// 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
    }
    // Dire si modifié avec succes
    return res.redirect('/recruiter/home');
  });
});

// Route pour supprimer une offre
router.get('/delete/:offreId', (req, res, next) => {
  const offreId = req.params.offreId;

  offreModel.delete(offreId, (err, affectedRows) => {
    if (err) {
      // Gérer l'erreur en rendant une vue d'erreur ou en redirigeant avec un message d'erreur
      return res.render('error', { message: 'Une erreur s\'est produite lors de la suppression de l\'offre.' });
    }

    if (affectedRows === 0) {
      // Aucune offre supprimée, afficher un message d'avertissement ou rediriger avec un message correspondant
      return res.redirect('/recruiter/home');
    }

    // Offre supprimée avec succès, rediriger vers la liste des offres ou afficher un message de succès
    res.redirect('/recruiter/home');
  });
});


router.get('/candidatures', function (req, res, next) {
  const id = req.session.organisation;
  if (!req.session.userId) {
    return res.redirect('/');
  }
  if (req.session.user_type !== 'recruteur') {
    return res.redirect('/');
  }
  result = candidModel.getByOrgaID(id, function (error, result) {
    if (error) {
      res.redirect('/recruiter/home');
    } else {
      res.render('Recruteur/CandidPage', { title: 'Homepage - offre', data: result });
    }
  });
});




router.get('/nvrecruteurs', function (req, res, next) {
  const id = req.session.userId;
  const orga = req.session.organisation;
  if (!req.session.userId) {
    return res.redirect('/');
  }
  if (req.session.user_type !== 'recruteur') {
    return res.redirect('/');
  }
  result = userModel.getByOrgaRecru(orga, function (error, result) {
    if (error) {
      res.redirect('/recruiter/home');
    } else {
      res.render('Recruteur/NouveauRecru', { title: 'Homepage - offre', data: result });
    }
  });
});

// Route accepter un nouveau recruteur
router.post('/acceptrecru/:id', (req, res, next) => {
  id = req.params.id;
  user = {
    statut: 'actif',
  }
  userModel.updateById(id, user, (err, affectedRows) => {
    if (err) {
      // Gérer l'erreur en rendant une vue d'erreur ou en redirigeant avec un message d'erreur
      return res.render('error', { message: 'Une erreur s\'est produite lors de la suppression de l\'offre.' });
    }

    if (affectedRows === 0) {
      // Aucunutilisateur modifié, afficher un message d'avertissement ou rediriger avec un message correspondant
      return res.redirect('/recruiter/home');
    }

    // Recruteur accepté avec succés
    res.redirect('/recruiter/home');
  });
});

// Route refuser un nouveau recruteur
router.post('/refurecru/:id', (req, res, next) => {
  id = req.params.id;
  user = {
    user_type: 'candidat',
    statut: 'actif',
    organisation: null
  }
  userModel.updateById(id, user, (err, affectedRows) => {
    if (err) {
      // Gérer l'erreur en rendant une vue d'erreur ou en redirigeant avec un message d'erreur
      return res.render('error', { message: 'Une erreur s\'est produite lors de la suppression de l\'offre.' });
    }

    if (affectedRows === 0) {
      // Aucune utilisateur modifié, afficher un message d'avertissement ou rediriger avec un message correspondant
      return res.redirect('/recruiter/home');
    }
    // Recruteur accepté avec succés
    res.redirect('/recruiter/home');
  });
});

router.get('/view-candid/:id', function (req, res) {
  // Récupérer l'ID de l'offre' à partir des paramètres de la requête
  const id = req.params.id;
  candidModel.getByOffreId(id, function (err, candidature) {
    if (err || !candidature) {
      // Gérer l'erreur ou la candidature introuvable
      res.status(404).send('Candidature non trouvée');
    } else {
      res.render('Recruteur/CandidPage', { title: 'Homepage - offre', data: candidature });
    }
  });
});

const path = require('path');

router.get('/candidatures/:id/telecharger', function (req, res) {
  // Récupérer l'ID de la candidature à partir des paramètres de la requête
  const id = req.params.id;
  candidModel.getByDocuId(id, function (err, candidature) {
    if (err || !candidature) {
      // Gérer l'erreur ou la candidature introuvable
      res.status(404).send('Candidature non trouvée');
    } else {
      // Récupérer les informations du fichier de la candidature
      const fichierCandidature = {
        chemin: candidature.chemin,
        nom: candidature.nom
      };
      // Vérifier si la candidature contient des fichiers
      if (!fichierCandidature.chemin || !fichierCandidature.nom) {
        res.status(404).send('Aucun fichier trouvé pour cette candidature');
      } else {
        // Envoyer le fichier au client pour téléchargement
        const parentDirPath = path.resolve(__dirname, '..');
        const filePath = path.join(parentDirPath, fichierCandidature.chemin);
        res.sendFile(filePath);
      }
    }
  });
});

router.get('/candidatures/:id', function (req, res, next) {
  const id = req.params.id;
  if (!req.session.userId) {
    return res.redirect('/');
  }
  if (req.session.user_type !== 'recruteur') {
    return res.redirect('/');
  }
  result = candidModel.getByCandidId(id, function (error, result) {
    if (error) {
      res.redirect('/recruiter/home');
    } else {
      res.render('Recruteur/FullCandid', { title: 'Homepage - offre', data: result });
    }
  });
});

router.post('/search-offer', function (req, res) {
  const userId = req.session.userId;
  const orgaid = req.session.organisation;
  if (!userId) {
    return res.redirect('/');
  }
  if (req.session.user_type !== 'recruteur') {
    return res.redirect('/');
  }

  const currentPage = req.body.page || 1; // Default to the first page if not specified
  const resultsPerPage = 5; // Specify the desired number of results per page
  const offset = (currentPage - 1) * resultsPerPage;
  const filters = {
    searchfield: req.body.searchfield || '',
  };
  offreModel.searchFilteredOffersRecru(filters, orgaid, resultsPerPage, offset, function (error, results) {
    if (error) {
      console.error('An error occurred during the search:', error);
      return res.render('Recruteur/RecruiterHomePage', {
        offre: [],
        currentPage: currentPage,
        totalPages: 1,
        error: error
      });
    }
    offreModel.countFilteredOffersRecru(filters, orgaid, function (error, totalCount) {
      if (error) {
        console.error('An error occurred while fetching the total count:', error);
        return res.render('Recruteur/RecruiterHomePage', {
          offre: [],
          currentPage: currentPage,
          totalPages: 1,
          error: error
        });
      }
      console.log(totalCount);
      console.log(results);
      const totalPages = Math.ceil(totalCount / resultsPerPage);
      console.log(totalPages);

      res.render('Recruteur/RecruiterHomePage', {
        offre: results,
        currentPage: currentPage,
        totalPages: totalPages,
        criteria: filters
      });
    });
  });
});

router.use('/common', common);


router.use('/profil', recruiterProfile);
module.exports = router;