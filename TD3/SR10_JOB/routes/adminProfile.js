var express = require('express');
var router = express.Router();
var userModel = require("../models/Utilisateur")
const pass = require('../public/javascripts/pass.js')




router.get('/', (req, res, next) => {
    const userId = req.session.userId;
    if (!userId) {
        // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
        return res.redirect('/');
    }
    if (req.session.user_type !== 'administrateur') {
        return res.redirect('/');
    }
    result = userModel.getById(userId, function (error, result) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }
        // Rendre la vue avec les détails de l'utilisateur
        res.render('Admin/MyInfo', { title: 'Utilisateur', data: result });

    });
});


router.get('/member', (req, res, next) => {
    const userId = req.session.userId;
    if (!userId) {
        // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
        return res.redirect('/');
    }
    if (req.session.user_type !== 'administrateur') {
        return res.redirect('/');
    }
    const Utilisateur = {
        nom: req.session.userName,
        prenom: req.session.userSurname,
    };
    res.render('Admin/BecomeMember', { title: 'BecomeRecruiter', data: Utilisateur });
});

// Route pour editer les détails de l'utilisateur courant
router.get('/edit', (req, res, next) => {
    const userId = req.session.userId;
    if (!userId) {
        // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
        return res.redirect('/');
    }
    if (req.session.user_type !== 'administrateur') {
        return res.redirect('/');
    }
    result = userModel.getById(userId, function (error, result) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }
        // Rendre la vue avec les détails de l'utilisateur
        res.render('Admin/EditMyInfo', { title: 'Utilisateur', data: result });
    });
});

router.post('/editing', function (req, res, next) {
    const userId = req.session.userId;
    if (!userId) {
        // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
        return res.redirect('/');
    }
    if (req.session.user_type !== 'administrateur') {
        return res.redirect('/');
    }
    
    const Utilisateur = {
        mail: req.body.email,
        nom: req.body.nom,
        phone: req.body.telephone,
        prenom: req.body.prenom,
        datecrea: req.body.datecrea
    };

    if (req.body.password) {
        const password = req.body.password;
        const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

        if (!passwordRegex.test(password)) {
            res.render('Admin/EditMyInfo', { data: Utilisateur, error: 'Le mot doit contenir 8 caractères dont au moins une majuscule, une minuscule, un chiffre, un caractère spécial'});
            return;
        }

        pass.generateHash(password, function (hash) {
            Utilisateur.password = hash;
            updateUtilisateur();
        });
    } else {
        updateUtilisateur();
    }

    function updateUtilisateur() {
        const id = req.session.userId;
        userModel.updateById(id, Utilisateur, function (error, updateResult) {
            if (error) {
                // Rendre la vue d'erreur avec le message d'erreur
                return res.render('Admin/EditMyInfo', { data: Utilisateur, error: error });
            }
            // Dire si crée avec succes
            return res.redirect('/admin/profil');
        });
    }
});



router.get('/member', (req, res, next) => {
    const userId = req.session.userId;
    if (!userId) {
        // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
        return res.redirect('/');
    }
    if (req.session.user_type !== 'administrateur') {
        return res.redirect('/');
    }
    res.render('Admin/BecomeMember', { title: 'BecomeRecruiter', data: req.session.user });
});
router.post('/member/become', function (req, res, next) {
    const userId = req.session.userId;
    const newRole = 'candidat';
    const newStatus = 'inactif';
    if (!userId) {
        // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
        return res.redirect('/');
    }
    if (req.session.user_type !== 'administrateur') {
        return res.redirect('/');
    }
    const Utilisateur = {
        nom: req.session.userName,
        prenom: req.session.userSurname,
    };
    // Vérifier s'il y a plus de 2 administrateurs
    userModel.getByStatusAndType('actif', 'administrateur', function (error, admins) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('Admin/BecomeMember', { data: Utilisateur, error: error });
        }
        console.log(admins);
        if (admins.length < 2) {
            // There are already 2 or fewer administrators, do not allow role change
            return res.render('Admin/BecomeMember', { data: Utilisateur, error: 'There must be at least 2 administrators.' });
        }

        // Update the user's role and status
        const updatedUser = {
            user_type: newRole,
            statut: newStatus
        };

        userModel.updateById(userId, updatedUser, function (error, updateResult) {
            if (error) {
                // Render the error view with the error message
                return res.render('Admin/BecomeMember', { data: Utilisateur, error: error });
            }

            // The update was successful
            return res.render('StatusChange');
        });
    });
});

module.exports = router;
