var express = require('express');
var router = express.Router();
var userModel = require("../models/Utilisateur")
var candidModel = require("../models/Candidature")
var orgaModel = require("../models/Organisation")
const pass = require('../public/javascripts/pass.js')


router.get('/', (req, res, next) => {
    const userId = req.session.userId;
    if (!userId) {
        // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
        return res.redirect('/');
    }

    if (req.session.user_type !== 'recruteur') {
        return res.redirect('/');
    }

    result = userModel.getById(userId, function (error, result) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }
        // Rendre la vue avec les détails de l'utilisateur
        res.render('Recruteur/Profil/MyInfo', { title: 'Utilisateur', data: result });
    });
});

// Route pour editer les détails de l'entreprise
router.get('/editFirm', (req, res, next) => {
    const userId = req.session.userId;
    const orga = req.session.organisation;
    if (!userId) {
        // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
        return res.redirect('/');
    }
    if (req.session.user_type !== 'recruteur') {
        return res.redirect('/');
    }
    result = orgaModel.getById(orga, function (error, result) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }
        // Rendre la vue avec les détails de l'entreprise
        res.render('Recruteur/Profil/EditFirm', { title: 'Utilisateur', data: result[0] });
    });
});

router.post('/editingFirm', function (req, res, next) {
    const Organisation = {
        siren: req.body.siren,
        name: req.body.name,
        head_office: req.body.adress,
        org_type: req.body.type
    };
    const id = req.session.organisation;
    //utiliser le model pour enregistrer les données récupérées dans la BD
    orgaModel.updateById(id, Organisation, function (error, updateResult) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('Recruteur/Profil/EditFirm', { data: Organisation, error: error });// 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }
        // Dire si modifié avec succes
        return res.redirect('/recruiter/profil/firm');
    });
});

// Route pour afficher les détails de l'organisation de l'utilisateur courant
router.get('/firm', (req, res, next) => {
    const userId = req.session.userId;
    const orga = req.session.organisation;
    if (!userId) {
        // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
        return res.redirect('/');
    }
    if (req.session.user_type !== 'recruteur') {
        return res.redirect('/');
    }

    result = orgaModel.getById(orga, function (error, result) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('Recruteur/Profil/MyInfo', { user: Utilisateur, orga: result, error: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }
        // Ajouter prenom et nom à result
        result[0].prenom = req.session.userSurname;
        result[0].nom = req.session.userName;
        // Rendre la vue avec les détails de l'organisation
        res.render('Recruteur/Profil/MyFirm', { title: 'Organisation', data: result[0] });
    });
});


router.get('/member', (req, res, next) => {
    const userId = req.session.userId;
    if (!userId) {
        return res.redirect('/');
    }
    if (req.session.user_type !== 'recruteur') {
        return res.redirect('/');
    }
    const Utilisateur = {
        nom: req.session.userName,
        prenom: req.session.userSurname,
    };
    res.render('Recruteur/Profil/BecomeMember', { title: 'BecomeMember', data: Utilisateur });
});

// Route pour devenir membre
router.post('/becomemember', (req, res, next) => {
    id = req.session.userId;
    user = {
        user_type: 'candidat',
        organisation: null
    };
    userModel.updateById(id, user, (err, affectedRows) => {
        if (err) {
            // Gérer l'erreur en rendant une vue d'erreur ou en redirigeant avec un message d'erreur
            return res.render('error', { message: 'Une erreur s\'est produite lors de la suppression de l\'offre.' });
        }

        if (affectedRows === 0) {
            // Aucun utilisateur modifié, afficher un message d'avertissement ou rediriger avec un message correspondant
            return res.redirect('/recruiter/home');
        }

        // membre accepté avec succés
        res.redirect('/member/home');
    });
});

// Route pour devenir admin
router.post('/becomeadmin', (req, res, next) => {
    id = req.session.userId;
    user = {
        user_type: 'administrateur',
        statut:'inactif'
    };
    userModel.updateById(id, user, (err, affectedRows) => {
        if (err) {
            // Gérer l'erreur en rendant une vue d'erreur ou en redirigeant avec un message d'erreur
            return res.render('error', { message: 'Une erreur s\'est produite lors de la suppression de l\'offre.' });
        }

        if (affectedRows === 0) {
            // Aucune offre supprimée, afficher un message d'avertissement ou rediriger avec un message correspondant
            return res.redirect('/recruiter/home');
        }

        // Recruteur accepté avec succés
        console.log(result);
        res.redirect('/admin/home');
    });
});

router.get('/admin', (req, res, next) => {
    const userId = req.session.userId;
    if (!userId) {
        return res.redirect('/');
    }
    if (req.session.user_type !== 'recruteur') {
        return res.redirect('/');
    }

    const Utilisateur = {
        nom: req.session.userName,
        prenom: req.session.userSurname,
    };
    res.render('Recruteur/Profil/BecomeAdmin', { title: 'BecomeAdmin', data: Utilisateur });

});

router.get('/candid/deletefile/:id', (req, res, next) => {
    const userId = req.session.userId;
    if (!userId) {
        return res.redirect('/');
    }
    if (req.session.user_type !== 'recruteur') {
        return res.redirect('/');
    }
    orgaModel.update(id, Organisation, function (error, updateResult) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('Recruteur/Profil/EditMyInfo', { data: Organisation, error: error });// 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }
        // Dire si modifié avec succes
        return res.redirect('/Recruteur/Profil/EditFirm');
    });

});

// Route pour editer les détails de l'utilisateur courant
router.get('/edit', (req, res, next) => {
    const userId = req.session.userId;
    if (!userId) {
        // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
        return res.redirect('/');
    }
    if (req.session.user_type !== 'recruteur') {
        return res.redirect('/');
    }
    result = userModel.getById(userId, function (error, result) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }
        // Rendre la vue avec les détails de l'utilisateur
        res.render('Recruteur/Profil/EditMyInfo', { title: 'Utilisateur', data: result });
    });
});

router.post('/editing', function (req, res, next) {
    const userId = req.session.userId;
    if (!userId) {
        // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
        return res.redirect('/');
    }
    if (req.session.user_type !== 'recruteur') {
        return res.redirect('/');
    }
    
    const Utilisateur = {
        mail: req.body.email,
        nom: req.body.nom,
        phone: req.body.telephone,
        prenom: req.body.prenom,
        datecrea: req.body.datecrea
    };

    if (req.body.password) {
        const password = req.body.password;
        const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

        if (!passwordRegex.test(password)) {
            res.render('Recruteur/EditMyInfo', { data: Utilisateur, error: 'Le mot doit contenir 8 caractères dont au moins une majuscule, une minuscule, un chiffre, un caractère spécial'});
            return;
        }

        pass.generateHash(password, function (hash) {
            Utilisateur.password = hash;
            updateUtilisateur();
        });
    } else {
        updateUtilisateur();
    }

    function updateUtilisateur() {
        const id = req.session.userId;
        userModel.updateById(id, Utilisateur, function (error, updateResult) {
            if (error) {
                // Rendre la vue d'erreur avec le message d'erreur
                return res.render('Recruteur/EditMyInfo', { data: Utilisateur, error: error });
            }
            // Dire si crée avec succes
            return res.redirect('/recruiter/profil');
        });
    }
});

module.exports = router;
