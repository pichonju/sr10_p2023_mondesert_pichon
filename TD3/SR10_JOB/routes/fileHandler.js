var express = require('express');
var router = express.Router();
var multer = require('multer');

var my_storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'mesfichiers');
  },
  filename: function (req, file, cb) {
    let my_extension = file.originalname.slice(file.originalname.lastIndexOf("."));
    cb(null, req.body.userId + '-' + req.body.myFileType + my_extension);
  }
});

var upload = multer({ storage: my_storage });

router.get('/candidature-offre/:id', function (req, res, next) {
    const id = req.params.id;
    
    const userId = req.session.userId;
    if (!userId) {
        return res.redirect('/');
    }
    if (req.session.user_type !== 'candidat') {
        return res.redirect('/');
    }

    if (req.session.uploaded_files == undefined) {
        console.log('Init uploaded files array');
        req.session.uploaded_files = [];
      }

    result = offreModel.getById(id, function (error, result) {
        console.log(result);
        res.render('Member/CandiOffer', { title: 'Homepage - offre', userId : userId, data: result[0], files_array: req.session.uploaded_files });
    });
});

/* POST */
router.post('/candidater-offre/:id', upload.single('cv'), function(req, res, next) {
  const uploaded_file = req.file;
  const userId = req.session.userId;
  if (!userId) {
      return res.redirect('/');
  }
  if (req.session.user_type !== 'candidat') {
      return res.redirect('/');
  }

  if (!uploaded_file) {
    res.render('file_upload', {
      connected_user: req.session.userId,
      files_array: req.session.uploaded_files,
      upload_error: 'Merci de sélectionner le fichier à charger !'
    });
  } else {
    console.log(uploaded_file.originalname, ' => ', uploaded_file.filename);
    req.session.uploaded_files.push(uploaded_file.filename);
    res.render('Member/CandiOffer', {
        userId: userId,
      files_array: req.session.uploaded_files,
      uploaded_filename: uploaded_file.filename,
      uploaded_original: uploaded_file.originalname
    });
  }
});

/* GET download */
router.get('/getfile', function(req, res, next) {
  try {
    res.download('./mesfichiers/' + req.query.fichier_cible);
  } catch (error) {
    res.send('Une erreur est survenue lors du téléchargement de ' + req.query.fichier_cible + ' : ' + error);
  }
});

module.exports = router;
