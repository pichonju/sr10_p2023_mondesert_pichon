var express = require('express');
var router = express.Router();
var userModel = require("../models/Utilisateur")
var adminProfile = require('./adminProfile');



router.get('/home', function (req, res, next) {
    if (req.session.user_type !== 'administrateur') {
        return res.redirect('/');
    }
 console.log( req.session.successMessage );
        userModel.getByStatusAndType("inactif", "candidat", function (error, result) {
            res.render('Admin/AdminHomePageNewM', { title: 'Homepage - admin', users: result});
        });
        delete req.session.successMessage;

    }
);

router.get('/home-admin', function (req, res, next) {
  if (req.session.user_type !== 'administrateur') {
      return res.redirect('/');
  }
console.log( req.session.successMessage );
      userModel.getByStatusAndType("inactif", "administrateur", function (error, result) {
          res.render('Admin/AdminHomePageNewA', { title: 'Homepage - admin', users: result});
      });
      delete req.session.successMessage;

  }
);

router.get('/home-recrut', function (req, res, next) {
  if (req.session.user_type !== 'administrateur') {
      return res.redirect('/');
  }
console.log( req.session.successMessage );
      userModel.getFirstRecruiters(function (error, result) {
          res.render('Admin/AdminHomePageNewRE', { title: 'Homepage - admin', users: result});
      });
      delete req.session.successMessage;

  }
);

router.post('/update-user/:id/:decision', function (req, res, next) {
    const id = req.params.id;
    const decision = req.params.decision;
    const orga =req.session.organisation;
  
    if (decision === 'approve') {
      const utilisateur = {
        statut: 'actif',
        organisation: null
      };

      userModel.updateById(id, utilisateur, function (error, result) {
        if (error) {
          req.flash('error', 'Une erreur a eu en acceptant cet utilisateur');
        }
        req.flash('success', 'Utilisateur accepté avec succès');

        return res.redirect('/admin/home');
      });

    } else if (decision === 'deny' && orga === null) {
      userModel.deleteById(id, function (error, result) {
        if (error) {
          req.flash('error', 'Une erreur a eu en supprimant cet utilisateur');
        }
        req.flash('success','Utilisateur supprimé avec succès');
        return res.redirect('/admin/home');
      });
    }else if (decision === 'deny' && orga !== null) {
      const utilisateur = {
        statut: 'actif',
        user_type:'recruteur'
      };

      userModel.updateById(id, utilisateur, function (error, result) {
        if (error) {
          req.flash('error', 'Une erreur a eu en acceptant cet utilisateur');
        }
        req.flash('success', 'Utilisateur accepté avec succès');

        return res.redirect('/admin/home');
      });
    } else {
      res.render('Admin/AdminHomePageNewM', { message: 'Décision invalide.' });
    }
  });


router.post('/search-user', function (req, res) {
  const criteria = req.body.criteria;
  const currentPage = req.body.page || 1; // Default to the first page if not specified
  const resultsPerPage = 5; // Specify the desired number of results per page
  const offset = (currentPage - 1) * resultsPerPage;

  userModel.searchUser(criteria, resultsPerPage, offset, function (error, results) {
    if (error) {
      console.error('An error occurred during the search:', error);
      return res.render('Admin/AdminHomePage', {
        users: [],
        currentPage: currentPage,
        totalPages: 1,
        error: error,
      });
    }

    // Calculate the total number of pages
    userModel.getUserCount(criteria, function (error, totalCount) {
      if (error) {
        console.error('An error occurred while fetching the total count:', error);
        return res.render('Admin/AdminHomePage', {
          users: [],
          currentPage: currentPage,
          totalPages: 1,
          error: error,
        });
      }

      const totalPages = Math.ceil(totalCount / resultsPerPage);
      res.render('Admin/AdminHomePage', {
        users: results,
        currentPage: currentPage,
        totalPages: totalPages,
        criteria : criteria
      });
    });
  });
});

router.use('/profil', adminProfile);

 module.exports = router;