var express = require('express');
var router = express.Router();
var userModel = require("../models/Utilisateur");
var candidModel = require("../models/Candidature");
var documentModel = require("../models/Document");
var organisationModel = require("../models/Organisation");
var docModel = require("../models/Document");
const fs = require('fs');
const pass = require('../public/javascripts/pass.js');
const session = require('express-session');



router.get('/', (req, res, next) => {
    const userId = req.session.userId;
    if (!userId) {
        // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
        return res.redirect('/');
    }

    if (req.session.user_type !== 'candidat') {
        return res.redirect('/');
    }

    result = userModel.getById(userId, function (error, result) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }
        // Rendre la vue avec les détails de l'utilisateur
        res.render('Member/Profil/MyInfo', { title: 'Utilisateur', data: result });
    });
});

router.get('/edit', (req, res, next) => {
    const userId = req.session.userId;
    if (!userId) {
        // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
        return res.redirect('/');
    }
    if (req.session.user_type !== 'candidat') {
        return res.redirect('/');
    }
    result = userModel.getById(userId, function (error, result) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('error', { message: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }
        // Rendre la vue avec les détails de l'utilisateur
        res.render('Member/Profil/EditMyInfo', { title: 'Utilisateur', data: result });
    });
});

router.post('/editing', function (req, res, next) {
    const userId = req.session.userId;
    if (!userId) {
        // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
        return res.redirect('/');
    }
    if (req.session.user_type !== 'candidat') {
        return res.redirect('/');
    }
    
    const Utilisateur = {
        mail: req.body.email,
        nom: req.body.nom,
        phone: req.body.telephone,
        prenom: req.body.prenom,
        datecrea: req.body.datecrea
    };

    if (req.body.password) {
        const password = req.body.password;
        const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

        if (!passwordRegex.test(password)) {
            res.render('Member/EditMyInfo', { data: Utilisateur, error: 'Le mot doit contenir 8 caractères dont au moins une majuscule, une minuscule, un chiffre, un caractère spécial'});
            return;
        }

        pass.generateHash(password, function (hash) {
            Utilisateur.password = hash;
            updateUtilisateur();
        });
    } else {
        updateUtilisateur();
    }

    function updateUtilisateur() {
        const id = req.session.userId;
        userModel.updateById(id, Utilisateur, function (error, updateResult) {
            if (error) {
                // Rendre la vue d'erreur avec le message d'erreur
                return res.render('Member/EditMyInfo', { data: Utilisateur, error: error });
            }
            // Dire si crée avec succes
            return res.redirect('/member/profil');
        });
    }
});


//#region Gestion des candidatures 
router.get('/candid', (req, res, next) => {
    const userId = req.session.userId;
    if (!userId) {
        // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
        return res.redirect('/');
    }
    if (req.session.user_type !== 'candidat') {
        return res.redirect('/');
    }

    const Utilisateur = {
        nom: req.session.userName,
        prenom: req.session.userSurname,
    };

    result = candidModel.getByCandidatId(userId, function (error, result) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('Member/Profil/MyCandid', { user: Utilisateur, candidatures: result, error: 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }
        // Rendre la vue avec les détails de l'utilisateur
        res.render('Member/Profil/MyCandid', { title: 'Utilisateur', user: Utilisateur, candidatures: result });
    });
});

router.get('/candid/edit/:id', (req, res) => {
    const candidatureId = req.params.id;
    const userId = req.session.userId;
    if (!userId) {
        return res.redirect('/');
    }
    if (req.session.user_type !== 'candidat') {
        return res.redirect('/');
    }

    // Retrieve the candidature data from the database based on the candidatureId
    result = candidModel.getById(candidatureId, (error, candidature) => {
        if (error) {
            return res.render('error', { error });
        }
        // Render the edit candidature form template and pass the candidature data to the template
        docs = docModel.getByCandidatAndOffer(candidatureId, (error, files) => {
            if (error) {
                // Handle the error case
                return res.render('error', { error });
            }
            // Render the edit candidature form template and pass the candidature data to the template
            const Utilisateur = {
                nom: req.session.userName,
                prenom: req.session.userSurname,
            };
            let docs = { cv: null, lm: null, cni: null };
            console.log(files);
            files.forEach(file => {
                if (file.type == "cv") {
                    docs.cv = { name: file.nom, id: file.id };
                }
                if (file.type == "lm") {
                    docs.lm = { name: file.nom, id: file.id };
                }
                if (file.type == "cni") {
                    docs.cni = { name: file.nom, id: file.id };
                }
            });
            console.log(docs);
            res.render('Member/Profil/EditMyCandi', { user: Utilisateur, data: candidature, files: docs });
        });
    });
});

router.get('/candid/delete/:id', (req, res) => {
    const candidatureId = req.params.id;
    const userId = req.session.userId;
    if (!userId) {
        return res.redirect('/');
    }
    if (req.session.user_type !== 'candidat') {
        return res.redirect('/');
    }

    // Retrieve the candidature data from the database based on the candidatureId
    docModel.deleteByCandidatureId(candidatureId, (error, files) => {
        if (error) {
            // Handle the error case
            return res.render('error', { error });
        }
        // Render the edit candidature form template and pass the candidature data to the template
        candidModel.deleteById(candidatureId, (error, files) => {
            if (error) {
                return res.render('error', { error });
            }
            const Utilisateur = {
                nom: req.session.userName,
                prenom: req.session.userSurname,
            };
            res.redirect('/member/profil/candid');
        });
    });
});

router.get('/candid/deletefile/:candi/:id', (req, res, next) => {
    const candidatureId = req.params.candi;
    const id = req.params.id;
    const userId = req.session.userId;
    if (!userId) {
        return res.redirect('/');
    }
    if (req.session.user_type !== 'candidat') {
        return res.redirect('/');
    }

    documentModel.getById(id, function (error, document) {
        if (error) {
            // Handle error here
            console.error(error);
            return res.redirect(`/member/profil/candid/edit/${candidatureId}`);
        }
        console.log(document);
        // Get the file path from the document or any other source
        const filePath = document.chemin;

        // Delete the file from the file system
        fs.unlink(filePath, function (err) {
            if (err) {
                // Handle error here
                console.error(err);
            } else {
                // File deletion successful
                console.log('File deleted successfully');
            }

            documentModel.deleteById(id, function (error) {
                return res.redirect(`/member/profil/candid/edit/${candidatureId}`)
            });
        });
    });
});
//#endregion

//#region Changement de rôle : Recruteur 
router.get('/recrut', (req, res, next) => {
    const userId = req.session.userId;
    if (!userId) {
        return res.redirect('/');
    }
    if (req.session.user_type !== 'candidat') {
        return res.redirect('/');
    }

    const Utilisateur = {
        nom: req.session.userName,
        prenom: req.session.userSurname,
    };
    res.render('Member/Profil/BecomeRecruiter', { title: 'BecomeAdmin', data: Utilisateur, currentPage: 1, totalPages: 0, criteria: null });
});

router.get('/recrut/existing/:org', function (req, res, next) {

    const org = req.params.org;
    const userId = req.session.userId;
    if (!userId) {
        return res.redirect('/');
    }
    if (req.session.user_type !== 'candidat') {
        return res.redirect('/');
    }
    const Utilisateur = {
        user_type: 'recruteur',
        statut: 'inactif',
        organisation: org
    };
    //utiliser le model pour enregistrer les données récupérées dans la BD
    userModel.updateById(userId, Utilisateur, function (error, updateResult) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('Member/Profil/BecomeAdmin', { data: Utilisateur, error: error });// 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }
        // Dire si crée avec succes
        return res.render('StatusChange');
    });
});

router.post('/search-firm', function (req, res) {
    const criteria = req.body.criteria;
    const currentPage = req.body.page || 1; // Default to the first page if not specified
    const resultsPerPage = 2; // Specify the desired number of results per page
    const offset = (currentPage - 1) * resultsPerPage;

    const Utilisateur = {
        nom: req.session.userName,
        prenom: req.session.userSurname,
    };

    organisationModel.searchOrganisation(criteria, resultsPerPage, offset, function (error, results) {
        if (error) {
            console.error('An error occurred during the search:', error);
            return res.render('Member/Profil/BecomeRecruiter', {
                results: results,
                currentPage: currentPage,
                totalPages: 1,
                error: error,
                data: Utilisateur
            });
        }

        // Calculate the total number of pages
        organisationModel.getOrganisationCount(criteria, function (error, totalCount) {
            if (error) {
                console.error('An error occurred while fetching the total count:', error);
                return res.render('Member/Profil/BecomeRecruiter', {
                    results: results,
                    currentPage: currentPage,
                    totalPages: totalPages,
                    error: error,
                    data: Utilisateur
                });
            }

            const totalPages = Math.ceil(totalCount / resultsPerPage);
            res.render('Member/Profil/BecomeRecruiter', {
                results: results,
                currentPage: currentPage,
                totalPages: totalPages,
                data: Utilisateur,
                criteria: criteria
            });
        });
    });
});

router.get('/new-firm', function (req, res) {
    const userId = req.session.userId;
    if (!userId) {
        return res.redirect('/');
    }
    if (req.session.user_type !== 'candidat') {
        return res.redirect('/');
    }
    const Utilisateur = {
        nom: req.session.userName,
        prenom: req.session.userSurname,
    };
    res.render('Member/Profil/NewOrganisation', { title: 'Utilisateur', user: Utilisateur });

});

router.post('/new-firm-registering', function (req, res, next) {
    const userId = req.session.userId;
    if (!userId) {
        // L'utilisateur n'est pas connecté, rediriger vers la page de connexion ou afficher un message d'erreur
        return res.redirect('/');
    }
    if (req.session.user_type !== 'candidat') {
        return res.redirect('/');
    }
    const Organisation = {
        siren: req.body.siren,
        name: req.body.name,
        head_office: req.body.adress,
        org_type: req.body.type
    };
    //utiliser le model pour enregistrer les données récupérées dans la BD
    organisationModel.create(Organisation, function (error, result) {
        if (error) {
            return res.render('Member/Profil/NewOrganisation', { error: error });// 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }
        // Dire si crée avec succes
        const Utilisateur = {
            user_type: 'recruteur',
            statut: 'inactif',
            organisation: Organisation.siren
        };
        //utiliser le model pour enregistrer les données récupérées dans la BD
        userModel.updateById(userId, Utilisateur, function (error, updateResult) {
            if (error) {
                // Rendre la vue d'erreur avec le message d'erreur
                return res.render('Member/Profil/BecomeAdmin', { user: Utilisateur, error: error });// 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
            }
            // Dire si crée avec succes
            return res.render('StatusChange');
        });
    });
});
//#endregion

//#region Changement de rôle : Administrateur 
router.get('/admin', (req, res, next) => {
    const userId = req.session.userId;
    if (!userId) {
        return res.redirect('/');
    }
    if (req.session.user_type !== 'candidat') {
        return res.redirect('/');
    }

    const Utilisateur = {
        nom: req.session.userName,
        prenom: req.session.userSurname,
    };
    res.render('Member/Profil/BecomeAdmin', { title: 'BecomeAdmin', data: Utilisateur });

});

router.post('/admin/become', function (req, res, next) {
    const Utilisateur = {
        user_type: 'administrateur',
        statut: 'inactif'
    };
    const id = req.session.userId;
    //utiliser le model pour enregistrer les données récupérées dans la BD
    userModel.updateById(id, Utilisateur, function (error, updateResult) {
        if (error) {
            // Rendre la vue d'erreur avec le message d'erreur
            return res.render('Member/Profil/BecomeAdmin', { data: Utilisateur, error: error });// 'Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur.' });
        }
        // Dire si crée avec succes
        return res.render('StatusChange');
    });
});
//#endregion

module.exports = router;
