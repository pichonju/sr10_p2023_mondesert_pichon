const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const session = require('express-session');
const flash = require('connect-flash');
const crypto = require('crypto');


const loginRouter = require('./routes/login');

const recruteurRouter = require('./routes/recruiter');
const memberRouter = require('./routes/member');
const recruiterRouter = require('./routes/recruiter');
const adminRouter = require('./routes/admin');

const app = express();

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


//Session
app.use(cookieParser());

const secret = crypto.randomBytes(32).toString('hex');
const deuxHeures = 1000 * 60 * 60 * 2;

app.use(session({
  secret: secret,
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false, maxAge: deuxHeures,httpOnly: true}
}));


app.use(flash());
app.use((req, res, next) => {
  res.locals.flash = {
    error: req.flash('error'),
    success: req.flash('success')
  };
  next();
});
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', loginRouter);
app.use('/member', memberRouter);
app.use('/recruiter', recruiterRouter);
app.use('/admin', adminRouter);
app.use('/recruiter', recruteurRouter);

// Catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});


app.use((req, res, next) => {
  // Définition de l'en-tête Content-Security-Policy (CSP)
  res.setHeader('Content-Security-Policy', "default-src 'self'");
  // Définition de l'en-tête X-XSS-Protection
  res.setHeader('X-XSS-Protection', '1; mode=block');
  // Appel du prochain middleware
  next();
});

// Error handler
app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
