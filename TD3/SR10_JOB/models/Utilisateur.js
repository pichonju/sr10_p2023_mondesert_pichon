
/* ********* Modèle pour la table Utilisateur ********* */

var db = require('./db.js'); 

const Utilisateur = {};

Utilisateur.create = (utilisateur, callback) => {
  db.query(
    'INSERT INTO Utilisateur (mail, password, nom, prenom, phone, datecrea, statut, user_type, organisation) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)',
    [
      utilisateur.mail,
      utilisateur.password,
      utilisateur.nom,
      utilisateur.prenom,
      utilisateur.phone,
      utilisateur.datecrea,
      utilisateur.statut,
      utilisateur.user_type,
      utilisateur.organisation
    ],
    (error, results, fields) => {
      if (error) {
        return callback(error, null);
      }
      return callback(null, results);
    }
  );
};


Utilisateur.getById = (id, callback) => {
  db.query('SELECT * FROM Utilisateur WHERE user_id = ?', [id], (error, results, fields) => {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results[0]);
  });
};

Utilisateur.getByStatus = (status, callback) => {
  db.query('SELECT * FROM Utilisateur WHERE statut = ?', [status], (error, results, fields) => {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results);
  });
};

Utilisateur.getByType = (type, callback) => {
  db.query('SELECT * FROM Utilisateur WHERE user_type = ?', [type], (error, results, fields) => {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results);
  });
};

Utilisateur.getByStatusAndType = (status, type, callback) => {
  db.query('SELECT * FROM Utilisateur WHERE statut = ? AND user_type = ?', [status, type], (error, results, fields) => {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results);
  });
};

Utilisateur.getFirstRecruiters = (callback) => {
  db.query(`SELECT * FROM Utilisateur u INNER JOIN Organisation o 
  WHERE u.statut = 'inactif' AND u.user_type = 'recruteur' AND o.siren = u.organisation 
  AND NOT EXISTS (SELECT 1 FROM Utilisateur WHERE statut = "actif" 
  AND user_type = "recruteur" AND organisation = u.organisation)
  `, 
    (error, results, fields) => {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results);
  });
};

Utilisateur.getByEmail = (mail, callback) => {
  db.query('SELECT * FROM Utilisateur WHERE mail = ?', [mail], (error, results, fields) => {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results);

  });
};

Utilisateur.getAll = (callback) => {
  db.query('SELECT * FROM Utilisateur', (error, results, fields) => {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results);
  });
};

Utilisateur.getUserCount = (criteria, callback) => {
  const countQuery = `
  SELECT COUNT(*) AS total_count
  FROM Utilisateur
  WHERE LOWER(nom) LIKE CONCAT('%', LOWER(?), '%')
    OR LOWER(prenom) LIKE CONCAT('%', LOWER(?), '%')
  `;
  const searchTerm = criteria || '';
  db.query(countQuery, [searchTerm,searchTerm], function (error, result) {
    if (error) {
      return callback(error, null);
    }
    const totalCount = parseInt(result[0].total_count);
    return callback(null, totalCount);
  });
},

Utilisateur.searchUser = (criteria, resultsPerPage, offset, callback) => {
const searchQuery = `
  SELECT *
  FROM Utilisateur
  WHERE LOWER(nom) LIKE CONCAT('%', LOWER(?), '%')
    OR LOWER(prenom) LIKE CONCAT('%', LOWER(?), '%')
  LIMIT ? OFFSET ?;
`;
  const searchTerm = criteria || '';

  db.query(searchQuery, [searchTerm, searchTerm, resultsPerPage, offset], function (error, results) {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results);
  });
};

Utilisateur.updateById = (id, utilisateur, callback) => {
  let updateFields = [];
  let query = 'UPDATE Utilisateur SET ';

  if (utilisateur.mail) {
    query += 'mail = ?, ';
    updateFields.push(utilisateur.mail);
  }

  if (utilisateur.password) {
    query += 'password = ?, ';
    updateFields.push(utilisateur.password);
  }

  if (utilisateur.nom) {
    query += 'nom = ?, ';
    updateFields.push(utilisateur.nom);
  }

  if (utilisateur.prenom) {
    query += 'prenom = ?, ';
    updateFields.push(utilisateur.prenom);
  }

  if (utilisateur.phone) {
    query += 'phone = ?, ';
    updateFields.push(utilisateur.phone);
  }

  if (utilisateur.statut) {
    query += 'statut = ?, ';
    updateFields.push(utilisateur.statut);
  }

  if (utilisateur.user_type) {
    query += 'user_type = ?, ';
    updateFields.push(utilisateur.user_type);
  }

  if (utilisateur.organisation) {
    query += 'organisation = ?, ';
    updateFields.push(utilisateur.organisation);
  }

  if (utilisateur.organisation===null) {
    query += 'organisation = ?, ';
    updateFields.push(utilisateur.organisation);
  }

  if (utilisateur.statut) {
    query += 'statut = ?, ';
    updateFields.push(utilisateur.statut);
  }

  // Add conditions for other fields that need to be updated

  // Remove the trailing comma and space from the query
  query = query.slice(0, -2);

  // Add the WHERE clause to specify the user_id
  query += ' WHERE user_id = ?';

  // Add the user_id to the updateFields array
  updateFields.push(id);

  db.query(query, updateFields, (error, results, fields) => {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results);
  });
};

Utilisateur.deleteById = (id, callback) => {
  db.query('DELETE FROM Utilisateur WHERE user_id = ?', [id], (error, results, fields) => {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results);
  });
};

Utilisateur.deleteByEmail = (mail, callback) => {
  db.query('DELETE FROM Utilisateur WHERE mail = ?', [mail], (error, results, fields) => {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results);
  });
};

Utilisateur.authenticateUser = (email, password, callback) => {
  db.query('SELECT * FROM Utilisateur WHERE mail = ? AND password = ? ', [email, password], (error, results, fields) => {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results[0]);
  });
};

Utilisateur.getByOrgaRecru = (orga, callback) => {
  db.query('SELECT * FROM Utilisateur WHERE organisation = ? AND statut = "inactif"', [orga], (error, results, fields) => {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results);
  });
};

module.exports = Utilisateur;