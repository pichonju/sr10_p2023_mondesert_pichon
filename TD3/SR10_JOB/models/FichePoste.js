/* ********* Modèle pour la table Offre ********* */

var db = require('./db.js'); 

const FichePoste = {};

// Créer une fiche de poste
FichePoste.createFichePoste = (fichePoste, callback) => {
  const sql = 'INSERT INTO Fiche_poste (intitule, description, type_metier, resp, loc, horaire, salairemin, salairemax, day_off, remote, offre_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
  const values = [fichePoste.intitule, fichePoste.description, fichePoste.type_metier, fichePoste.resp, fichePoste.loc, fichePoste.horaire, fichePoste.salairemin, fichePoste.salairemax, fichePoste.day_off, fichePoste.remote, fichePoste.offre_id];
  db.query(sql, values, callback);
};

// Récupérer toutes les fiches de poste
FichePoste.getAll = (callback) => {
  db.query('SELECT * FROM Fiche_poste', (error, results, fields) => {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results);
  });
};


// Récupérer une fiche de poste par son ID
FichePoste.getFichePosteById = (id, callback) => {
  const sql = 'SELECT * FROM Fiche_poste WHERE poste_id = ?';
  db.query(sql, [id], callback);
};

// Mettre à jour une fiche de poste
FichePoste.updateFichePoste = (fichePoste, callback) => {
  const sql = 'UPDATE Fiche_poste SET intitule = ?, description = ?, status = ?, resp = ?, type_metier = ?, loc = ?, horaire = ?, salairemin = ?, salairemax = ?, day_off = ?, remote = ?, offre_id = ? WHERE poste_id = ?';
  const values = [fichePoste.intitule, fichePoste.description, fichePoste.status, fichePoste.resp, fichePoste.type_metier, fichePoste.loc, fichePoste.horaire, fichePoste.salairemin, fichePoste.salairemax, fichePoste.day_off, fichePoste.remote, fichePoste.offre_id, fichePoste.poste_id];
  db.query(sql, values, callback);
};

// Supprimer une fiche de poste par son ID
FichePoste.deleteFichePoste = (id, callback) => {
  const sql = 'DELETE FROM Fiche_poste WHERE poste_id = ?';
  db.query(sql, [id], callback);
};

FichePoste.updateById = (offre_id, updatedFiche, callback) => {
  let updateFields = [];
  let query = 'UPDATE Fiche_poste SET ';

  if (updatedFiche.intitule) {
    query += 'intitule = ?, ';
    updateFields.push(updatedFiche.intitule);
  }

  if (updatedFiche.description) {
    query += 'description = ?, ';
    updateFields.push(updatedFiche.description);
  }

  if (updatedFiche.resp) {
    query += 'resp = ?, ';
    updateFields.push(updatedFiche.resp);
  }

  if (updatedFiche.type_metier) {
    query += 'type_metier = ?, ';
    updateFields.push(updatedFiche.type_metier);
  }

  if (updatedFiche.loc) {
    query += 'loc = ?, ';
    updateFields.push(updatedFiche.loc);
  }

  if (updatedFiche.horaire) {
    query += 'horaire = ?, ';
    updateFields.push(updatedFiche.horaire);
  }

  if (updatedFiche.salairemin) {
    query += 'salairemin = ?, ';
    updateFields.push(updatedFiche.salairemin);
  }

  if (updatedFiche.salairemax) {
    query += 'salairemax = ?, ';
    updateFields.push(updatedFiche.salairemax);
  }

  if (updatedFiche.day_off) {
    query += 'day_off = ?, ';
    updateFields.push(updatedFiche.day_off);
  }

  if (updatedFiche.remote) {
    query += 'remote = ?, ';
    updateFields.push(updatedFiche.remote);
  }
  // Add conditions for other fields that need to be updated

  // Remove the trailing comma and space from the query
  query = query.slice(0, -2);

  // Add the WHERE clause to specify the user_id
  query += ' WHERE offre_id = ?';

  // Add the user_id to the updateFields array
  updateFields.push(offre_id);

  db.query(query, updateFields, (error, results, fields) => {
    console.log(query);
    console.log(updatedFiche);
    if (error) {
      return callback(error, null);
    }
    return callback(null, results);
  });
};


module.exports = FichePoste;
