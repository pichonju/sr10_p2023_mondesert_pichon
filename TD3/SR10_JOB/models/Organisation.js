/* ********* Modèle pour la table Candidature ********* */

var db = require('./db.js');

const Organisation = {};

Organisation.create = (newOrganisation, callback) => {
  db.query('INSERT INTO Organisation SET ?', newOrganisation, (error, results, fields) => {
    if (error) {
      callback(error, null);
    } else {
      callback(null, results);
    }
  });
};

Organisation.getById = (siren, callback) => {
  db.query('SELECT * FROM Organisation WHERE siren = ?', siren, (error, results, fields) => {
    if (error) {
      callback(error, null);
    } else {
      callback(null, results);
    }
  });
};

Organisation.getByNom = (nom, callback) => {
  db.query('SELECT * FROM Organisation WHERE name = ?', nom, (error, results, fields) => {
    if (error) {
      callback(error, null);
    } else {
      callback(null, results);
    }
  });
};

Organisation.getByType = (org_type, callback) => {
  db.query('SELECT * FROM Organisation WHERE org_type = ?', org_type, (error, results, fields) => {
    if (error) {
      callback(error, null);
    } else {
      callback(null, results);
    }
  });
};

Organisation.getByLocation = (head_office, callback) => {
  db.query('SELECT * FROM Organisation WHERE head_office = ?', head_office, (error, results, fields) => {
    if (error) {
      callback(error, null);
    } else {
      callback(null, results);
    }
  });
};

Organisation.getAll = (callback) => {
  db.query('SELECT * FROM Organisation', (error, results, fields) => {
    if (error) {
      callback(error, null);
    } else {
      callback(null, results);
    }
  });
};

Organisation.searchOrganisation = (criteria, resultsPerPage, offset, callback) => {
  const searchQuery = `
    SELECT *
    FROM Organisation
    WHERE (LOWER(name) LIKE CONCAT('%', LOWER(?), '%') OR siren = ?)
    LIMIT ? OFFSET ?;
  `;

  // Check if criteria is null or empty
  const searchTerm = criteria || '';

  db.query(searchQuery, [searchTerm, searchTerm, resultsPerPage, offset], function (error, results) {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results);
  });
};


  Organisation.getOrganisationCount = (criteria, callback) => {
    const countQuery = `
    SELECT COUNT(*) AS total_count
    FROM Organisation
    WHERE LOWER(name) LIKE CONCAT('%', LOWER(?), '%')
      OR siren = ?
  `;
    const searchTerm = criteria || '';
    db.query(countQuery, [searchTerm,searchTerm], function (error, result) {
      if (error) {
        return callback(error, null);
      }
      const totalCount = parseInt(result[0].total_count);
      return callback(null, totalCount);
    });
  },

Organisation.searchOrganisation = (criteria, resultsPerPage, offset, callback) => {
  const searchQuery = `
    SELECT *
    FROM Organisation
    WHERE (LOWER(name) LIKE CONCAT('%', LOWER(?), '%') OR siren = ?)
    LIMIT ? OFFSET ?;
  `;

  // Check if criteria is null or empty
  const searchTerm = criteria || '';

  db.query(searchQuery, [searchTerm, searchTerm, resultsPerPage, offset], function (error, results) {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results);
  });
};


  Organisation.getOrganisationCount = (criteria, callback) => {
    const countQuery = `
    SELECT COUNT(*) AS total_count
    FROM Organisation
    WHERE LOWER(name) LIKE CONCAT('%', LOWER(?), '%')
      OR siren = ?
  `;
    const searchTerm = criteria || '';
    db.query(countQuery, [searchTerm,searchTerm], function (error, result) {
      if (error) {
        return callback(error, null);
      }
      const totalCount = parseInt(result[0].total_count);
      return callback(null, totalCount);
    });
  },


  Organisation.updateById = (siren, updatedOrganisation, callback) => {
    let updateFields = [];
    let query = 'UPDATE Organisation SET ';
  
    if (updatedOrganisation.name) {
      query += 'name = ?, ';
      updateFields.push(updatedOrganisation.name);
    }
  
    if (updatedOrganisation.siren) {
      query += 'siren = ?, ';
      updateFields.push(updatedOrganisation.siren);
    }
  
    if (updatedOrganisation.org_type) {
      query += 'org_type = ?, ';
      updateFields.push(updatedOrganisation.org_type);
    }
  
    if (updatedOrganisation.head_office) {
      query += 'head_office = ?, ';
      updateFields.push(updatedOrganisation.head_office);
    }
    // Add conditions for other fields that need to be updated
  
    // Remove the trailing comma and space from the query
    query = query.slice(0, -2);
  
    // Add the WHERE clause to specify the user_id
    query += ' WHERE siren = ?';
  
    // Add the user_id to the updateFields array
    updateFields.push(siren);
  
    db.query(query, updateFields, (error, results, fields) => {
      console.log(query);
      console.log(updatedOrganisation);
      if (error) {
        return callback(error, null);
      }
      return callback(null, results);
    });
  };
  
  Organisation.update = (siren, updatedOrganisation, callback) => {
    db.query('UPDATE Organisation SET ? WHERE siren = ?', [updatedOrganisation, siren], (error, results, fields) => {
      if (error) {
        callback(error, null);
      } else {
        callback(null, results);
      }
    });
  };

Organisation.delete = (siren, callback) => {
  db.query('DELETE FROM Organisation WHERE siren = ?', siren, (error, results, fields) => {
    if (error) {
      callback(error, null);
    } else {
      callback(null, results);
    }
  });
};

module.exports = Organisation;
