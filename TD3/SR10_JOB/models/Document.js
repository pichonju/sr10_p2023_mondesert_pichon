var db = require('./db.js');

const Document = {};
Document.create = (document, callback) => {
  db.query(
    'INSERT INTO Document (candidature, chemin, type, nom) VALUES (?, ?, ?, ?)',
    [document.candidature, document.chemin, document.type, document.nom],
    (error, results, fields) => {
      if (error) {
        return callback(error, null, null); // Pass null for documentId and documentName
      }
      const documentId = results.insertId;
      const documentName = document.nom;
      return callback(null, documentId, documentName);
    }
  );
};

Document.createMultiple = (documents, callback) => {
  const values = documents.map(document => [
    document.candidature,
    document.chemin,
    document.type,
    document.nom
  ]);

  db.query(
    'INSERT INTO Document (candidature, chemin, type, nom) VALUES ?',
    [values],
    (error, results, fields) => {
      if (error) {
        return callback(error, null, null); // Pass null for documentIds and documentNames
      }
      
      const documentIds = [];
      const documentNames = [];
      for (let i = 0; i < results.affectedRows; i++) {
        documentIds.push(results.insertId + i);
        documentNames.push(documents[i].nom);
      }

      return callback(null, documentIds, documentNames);
    }
  );
};


Document.getByCandidatAndOffer = (candidature, callback) => {
  db.query(
    'SELECT * FROM Document WHERE candidature = ? ',
    [candidature],
    (error, results, fields) => {
      if (error) {
        return callback(error, null);
      }
      return callback(null, results);
    }
  );
};

// Retrieve documents by offer
Document.getByOffer = (offerId, callback) => {
  db.query(
    'SELECT * FROM Document WHERE offre = ?',
    [offerId],
    (error, results, fields) => {
      if (error) {
        return callback(error, null);
      }
      return callback(null, results);
    }
  );
};

Document.deleteById = (documentId, callback) => {
  db.query(
    'DELETE FROM Document WHERE id = ?',
    [documentId],
    (error, results, fields) => {
      if (error) {
        return callback(error);
      }
      return callback(null);
    }
  );
};

Document.deleteByCandidatureId = (documentId, callback) => {
  db.query(
    'DELETE FROM Document WHERE candidature = ?',
    [documentId],
    (error, results, fields) => {
      if (error) {
        return callback(error);
      }
      return callback(null);
    }
  );
};

Document.getById = (documentId, callback) => {
  db.query(
    'SELECT * FROM Document WHERE id = ?',
    [documentId],
    (error, results, fields) => {
      if (error) {
        return callback(error, null);
      }
      return callback(null, results[0]);
    }
  );
};
module.exports = Document;
