var db = require('./db.js');
var Document = require('./Document.js');

const Candidature = {};

Candidature.create = (candidature, callback) => {
  const fichiers = [];
  if (candidature.files.cv) {
    fichiers.push(candidature.files.cv);
  }
  
  if (candidature.files.lm  !== null) {
    fichiers.push(candidature.files.lm);
  }
  
  if (candidature.files.cni !== null) {
    fichiers.push(candidature.files.cni);
  }  
  db.query(
    'INSERT INTO Candidature (candidat, offre, date) VALUES (?, ?, ?)',
    [candidature.candidat, candidature.offre, candidature.date],
    (error, results, fields) => {
      if (error) {
        return callback(error, null);
      }

      const candidatureId = results.insertId;
      let docs = 0;
      console.log(fichiers);
      // Create pieces dossiers if provided
      fichiers.forEach(file => {
        if (file !== null) {
          const doc = {
            nom: file.nom,
            chemin: file.chemin,
            type: file.type,
            candidature: candidatureId, // Associate each piece with the candidature ID
          };
          console.log(doc);
          Document.create(doc, (error, documentId, documentName) => {
            if (error) {
              return callback(error, null);
            }
            docs++;
            if (docs === fichiers.length) {
              // All documents have been created, call the callback with the candidature ID and docs count
              return callback(null, candidatureId, docs);
            }
          });
        }
      });
      // If no documents were provided, call the callback with the candidature ID and docs count as 0
      if (fichiers.length === 0) {
        return callback(null, candidatureId, 0);
      }
    }
  );
};

Candidature.getById = (candidatureId, callback) => {
  const query = `
  SELECT Candidature.*, Offre.*, Fiche_poste.*
  FROM Candidature
  INNER JOIN Offre ON Candidature.offre = Offre.offre_id
  INNER JOIN Fiche_poste ON Offre.offre_id = Fiche_poste.offre_id
  WHERE Candidature.id = ?
  `;
  db.query(query, [candidatureId], (err, result) => {
    if (err) return callback(err);
    callback(null, result[0]);
  });
};


Candidature.getByCandidatId = (candidatId, callback) => {
  const query = `
    SELECT Candidature.*, Offre.*, Fiche_poste.*
    FROM Candidature
    INNER JOIN Offre ON Candidature.offre = Offre.offre_id
    INNER JOIN Fiche_poste ON Offre.offre_id = Fiche_poste.offre_id
    WHERE Candidature.candidat = ?
  `;
  db.query(query, [candidatId], (err, result) => {
    if (err) return callback(err);
    callback(null, result);
  });
};

Candidature.getByOrgaID = (id, callback) => {
  const query = `
  SELECT Candidature.*, Offre.*, Fiche_poste.*, Utilisateur.*
  FROM Candidature
  INNER JOIN Offre ON Candidature.offre = Offre.offre_id
  INNER JOIN Fiche_poste ON Offre.offre_id = Fiche_poste.offre_id
  INNER JOIN Utilisateur ON Candidature.candidat = Utilisateur.user_id
  WHERE Offre.organisation = ?
  GROUP BY Candidature.id
  `;
  db.query(query, [id], (err, result) => {
    if (err) return callback(err);
    if (!result || result.length === 0) {
      return callback(null, []); // Retourner un tableau vide si aucune candidature n'est trouvée
    }
    callback(null, result);
});
};

Candidature.getByCandidId = (id, callback) => {
  const query = `
  SELECT Document.*,Document.nom AS document_name, Utilisateur.*, Fiche_poste.*
  FROM Document
  JOIN Candidature ON Candidature.id = Document.candidature
  JOIN Utilisateur ON Utilisateur.user_id = Candidature.candidat
  JOIN Fiche_poste ON Fiche_poste.offre_id = Candidature.offre
  WHERE Candidature.id = ?;
  `;
  db.query(query, [id], (err, result) => {
    if (err) return callback(err);
    if (!result || result.length === 0) {
      return callback(null, []); // Retourner un tableau vide si aucune candidature n'est trouvée
    }
    callback(null, result);
});
};

Candidature.getByOffreId = (candidatureId, callback) => {
  const query = `
  SELECT Document.*, Candidature.*, Utilisateur.*, Fiche_poste.*
  FROM Candidature
  JOIN Document ON Document.candidature = Candidature.id 
  JOIN Utilisateur ON Utilisateur.user_id = Candidature.candidat
  JOIN Fiche_poste ON Fiche_poste.offre_id = Candidature.offre
  WHERE Candidature.offre = ?
  GROUP BY Document.candidature
  `;
  db.query(query, [candidatureId], (err, result) => {
    if (err) return callback(err);
    callback(null, result);
  });
};


Candidature.getByDocuId = (candidatureId, callback) => {
  const query = `
  SELECT Candidature.*, Offre.*, Fiche_poste.*, Document.*
  FROM Candidature
  INNER JOIN Offre ON Candidature.offre = Offre.offre_id
  INNER JOIN Fiche_poste ON Offre.offre_id = Fiche_poste.offre_id
  INNER JOIN Document ON Candidature.id = Document.candidature

  WHERE Document.id = ?
  `;
  db.query(query, [candidatureId], (err, result) => {
    if (err) return callback(err);
    callback(null, result[0]);
  });
};

Candidature.deleteById= (candidat_id, callback) => {
  db.query(
    'DELETE FROM Candidature WHERE id = ? ',
    [candidat_id],
    (error, results, fields) => {
      if (error) {
        return callback(error, null);
      }
      return callback(null, results);
    }
  );
};

module.exports = Candidature;
