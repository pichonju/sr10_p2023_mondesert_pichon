/* ********* Modèle pour la table Offre ********* */

var db = require('./db.js');

const fichePostemodel = require("../models/FichePoste.js");
const Offre = {};


Offre.create = (state, organisation, recruteur, callback) => {
  const query = 'INSERT INTO Offre (state, organisation, recruteur) VALUES (?, ?, ?)';
  db.query(query, [state, organisation, recruteur], (err, result) => {
    if (err) return callback(err);
    callback(null, result.insertId);
  });
};

Offre.getAll = (callback) => {
  const query = 'SELECT * FROM Offre INNER JOIN Fiche_poste INNER JOIN Organisation WHERE Fiche_poste.offre_id=Offre.offre_id AND Organisation.siren=Offre.organisation   ';
  db.query(query, (err, result) => {
    if (err) return callback(err);
    callback(null, result);
  });
};

Offre.getByState = (state, callback) => {
  const query = 'SELECT * FROM Offre INNER JOIN Fiche_poste INNER JOIN Organisation WHERE Fiche_poste.offre_id=Offre.offre_id AND Organisation.siren=Offre.organisation AND Offre.state = ?  ';
  db.query(query, [state], (err, result) => {
    if (err) return callback(err);
    callback(null, result);
  });
};

Offre.getNotAppliedByUserOffer = (userid,  limit,callback) => {
  const query = `
  SELECT *
  FROM Offre
  INNER JOIN Fiche_poste
  INNER JOIN Organisation
  WHERE Fiche_poste.offre_id = Offre.offre_id
    AND Organisation.siren = Offre.organisation
    AND Offre.state = 'publiee'
    AND Offre.date_v_fin > CURDATE()
    AND Offre.offre_id NOT IN (
      SELECT offre
      FROM Candidature
      WHERE candidat = ?
    )
    Order by Offre.date_v_debut ASC
    LIMIT ?;
`;
  db.query(query, [userid, limit], (err, result) => {
    if (err) return callback(err);
    callback(null, result);
  });
};

Offre.getByOrganisation = (org, callback) => {
  const query = 'SELECT * FROM Offre INNER JOIN Fiche_poste INNER JOIN Organisation WHERE Fiche_poste.offre_id=Offre.offre_id AND Organisation.siren=Offre.organisation AND Offre.organisation = ?';
  db.query(query, [org], (err, result) => {
    if (err) return callback(err);
    callback(null, result);
  });
};

Offre.getById = (id, callback) => {
  const query = 'SELECT * FROM Offre INNER JOIN Fiche_poste INNER JOIN Organisation WHERE Fiche_poste.offre_id=Offre.offre_id AND Organisation.siren=Offre.organisation AND Offre.offre_id = ?';
  db.query(query, [id], (err, result) => {
    if (err) return callback(err);
    if (result.length === 0) return callback({ message: 'Offre not found' });
    callback(null, result);
  });
};

Offre.update = (id, state, organisation, recruteur, callback) => {
  const query = 'UPDATE Offre SET state = ?, organisation = ?, recruteur = ? WHERE offre_id = ?';
  db.query(query, [state, organisation, recruteur, id], (err, result) => {
    if (err) return callback(err);
    callback(null, result.changedRows);
  });
};

Offre.delete = (id, callback) => {
  const query = 'DELETE FROM Offre WHERE offre_id = ?';
  db.query(query, [id], (err, result) => {
    if (err) return callback(err);
    callback(null, result.affectedRows);
  });
};

Offre.delete = (id, callback) => {
  const deleteQuery = `
    DELETE Offre, Fiche_poste
    FROM Offre
    LEFT JOIN Fiche_poste ON Offre.offre_id = Fiche_poste.offre_id
    WHERE Offre.offre_id = ?
  `;

  db.query(deleteQuery, [id], (err, result) => {
    if (err) return callback(err);
    console.log(result);
    callback(null, result.affectedRows);
  });
};

Offre.getOffresWithFilter = (location, minSalary, maxSalary, salary, callback) => {
    let query = 'SELECT * FROM Offre o JOIN Fiche_poste f ON o.offre_id = f.offre_id WHERE 1=1';

    if (location) {
      query += ` AND o.organisation IN (SELECT siren FROM Organisation WHERE lower(head_office) LIKE lower('%${location}%'))`;
    }

    if (minSalary) {
      query += ` AND f.salairemin >= ${minSalary}`;
    }

    if (maxSalary) {
      query += ` AND f.salairemax <= ${maxSalary}`;
    }

    if (salary) {
      query += ` AND (f.salairemin <= ${salary} OR f.salairemax >= ${salary})`;
    }

    db.query(query, (err, result) => {
      if (err) return callback(err);
      if (result.length === 0) return callback({ message: 'Offres not found' });
      callback(null, result);
    });
};

Offre.searchFilteredOffers = (filters, userid, resultsPerPage, offset, callback) => {
  let searchFields = [];
  let query = `SELECT Offre.*, Fiche_poste.*, Organisation.*
    FROM Offre INNER JOIN Fiche_poste ON Offre.offre_id = Fiche_poste.offre_id
    INNER JOIN Organisation ON Organisation.siren = Offre.organisation
    WHERE`;

  if (filters.salaire) {
    query += ' ? >= Fiche_poste.salairemin AND ? <= Fiche_poste.salairemax AND ';
    searchFields.push(filters.salaire);
    searchFields.push(filters.salaire);
  }

  if (filters.head_office) {
    query += ' Organisation.head_office = ? AND ';
    searchFields.push(filters.head_office);
  }

  if (filters.remote) {
    query += ' Fiche_poste.remote = ? AND ';
    searchFields.push(filters.remote);
  }

  if (filters.dayoff) {
    query += ' Fiche_poste.day_off  >= ? AND ';
    searchFields.push(filters.dayoff);
  }

    query += ` (LOWER(Fiche_poste.intitule) LIKE CONCAT(\'%\', LOWER(?), \'%\') OR 
    LOWER(Fiche_poste.description) LIKE CONCAT(\'%\', LOWER(?), \'%\') OR
    LOWER(Fiche_poste.type_metier) LIKE CONCAT(\'%\', LOWER(?), \'%\')) AND`

    searchFields.push(filters.searchfield);
    searchFields.push(filters.searchfield);
    searchFields.push(filters.searchfield);

  if (filters.org) {
    query += ' Fiche_poste.loc = ? AND';
    searchFields.push(filters.org);
  }

  query += ' Offre.state = \'publiee\' AND Offre.date_v_fin > CURDATE() ';
  query += ` AND Offre.offre_id NOT IN (
    SELECT offre
    FROM Candidature
    WHERE candidat = ?
  )`
  searchFields.push(userid);
  query += '  LIMIT ? OFFSET ?;';
  searchFields.push(resultsPerPage);
  searchFields.push(offset);

  db.query(query, searchFields, (error, results, fields) => {
    if (error) {
      return callback(error, null);
    }
    console.log(query);
    console.log(searchFields);
    return callback(null, results);
  });
};


Offre.countFilteredOffers = (filters, userid, callback) => {
  let query = `
  SELECT COUNT(*) AS total_count
  FROM Offre INNER JOIN Fiche_poste ON Offre.offre_id = Fiche_poste.offre_id
  INNER JOIN Organisation ON Organisation.siren = Offre.organisation  WHERE`;
  let searchFields = [];


  if (filters.salaire) {
    query += ' ? >= Fiche_poste.salairemin AND ? <= Fiche_poste.salairemax AND ';
    searchFields.push(filters.salaire);
    searchFields.push(filters.salaire);
  }

  if (filters.head_office) {
    query += ' Organisation.head_office = ? AND ';
    searchFields.push(filters.head_office);
  }

  if (filters.remote) {
    query += ' Fiche_poste.remote = ? AND ';
    searchFields.push(filters.remote);
  }

  if (filters.dayoff) {
    query += ' Fiche_poste.day_off  >= ? AND ';
    searchFields.push(filters.dayoff);
  }

    query += ` (LOWER(Fiche_poste.intitule) LIKE CONCAT(\'%\', LOWER(?), \'%\') OR 
    LOWER(Fiche_poste.description) LIKE CONCAT(\'%\', LOWER(?), \'%\') OR
    LOWER(Fiche_poste.type_metier) LIKE CONCAT(\'%\', LOWER(?), \'%\')) AND`

    searchFields.push(filters.searchfield);
    searchFields.push(filters.searchfield);
    searchFields.push(filters.searchfield);

  if (filters.org) {
    query += ' Fiche_poste.loc = ? AND';
    searchFields.push(filters.org);
  }

  query += ' Offre.state = \'publiee\' AND Offre.date_v_fin > CURDATE() ';
  query += ` AND Offre.offre_id NOT IN (
    SELECT offre
    FROM Candidature
    WHERE candidat = ?
  )`
  searchFields.push(userid);

  db.query(query, searchFields, function (error, result) {
    if (error) {
      return callback(error, null);
    }
    const totalCount = parseInt(result[0].total_count);
    return callback(null, totalCount);
  });
},

Offre.searchFilteredOffersRecru = (filters, orgaid, resultsPerPage, offset, callback) => {
  let searchFields = [];
  let query = `
    SELECT Offre.*, Fiche_poste.*, Organisation.*
    FROM Offre
    INNER JOIN Fiche_poste ON Offre.offre_id = Fiche_poste.offre_id
    INNER JOIN Organisation ON Organisation.siren = Offre.organisation
    WHERE Organisation.siren = ?`;

  searchFields.push(orgaid);

  if (filters.searchfield) {
    query += ` AND (
      LOWER(Fiche_poste.intitule) LIKE CONCAT('%', LOWER(?), '%') OR
      LOWER(Fiche_poste.description) LIKE CONCAT('%', LOWER(?), '%') OR
      LOWER(Fiche_poste.type_metier) LIKE CONCAT('%', LOWER(?), '%')
    )`;

    searchFields.push(filters.searchfield);
    searchFields.push(filters.searchfield);
    searchFields.push(filters.searchfield);
  }

  query += ' LIMIT ? OFFSET ?;';
  searchFields.push(resultsPerPage);
  searchFields.push(offset);

  db.query(query, searchFields, (error, results, fields) => {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results);
  });
};

Offre.countFilteredOffersRecru = (filters, orgaid, callback) => {
  let query = `
    SELECT COUNT(*) AS total_count
    FROM Offre
    INNER JOIN Fiche_poste ON Offre.offre_id = Fiche_poste.offre_id
    INNER JOIN Organisation ON Organisation.siren = Offre.organisation
    WHERE Organisation.siren = ?`;

  let searchFields = [orgaid];

  if (filters.searchfield) {
    query += ` AND (
      LOWER(Fiche_poste.intitule) LIKE CONCAT('%', LOWER(?), '%') OR
      LOWER(Fiche_poste.description) LIKE CONCAT('%', LOWER(?), '%') OR
      LOWER(Fiche_poste.type_metier) LIKE CONCAT('%', LOWER(?), '%')
    )`;

    searchFields.push(filters.searchfield);
    searchFields.push(filters.searchfield);
    searchFields.push(filters.searchfield);
  }

  db.query(query, searchFields, function (error, result) {
    if (error) {
      return callback(error, null);
    }
    const totalCount = parseInt(result[0].total_count);
    return callback(null, totalCount);
  });
};


Offre.createWithFiche = (offreData, fichePosteData, callback) => {
  db.query(
    'INSERT INTO Offre (state, organisation, date_v_debut, date_v_fin) VALUES (?, ?, ?, ?)',
    [offreData.state, offreData.organisation, offreData.date_v_debut, offreData.date_v_fin],
    (err, result) => {
      if (err) {
        callback(err);
        return;
      }

      const offreId = result.insertId;

      const fichePosteDataWithOffreId = {
        ...fichePosteData,
        offre_id: offreId
      };

      fichePostemodel.createFichePoste(fichePosteDataWithOffreId, (err, result) => {
        if (err) {
          callback(err);
          return;
        }else {
          return callback(err, result);
        }      
      }
    );
  });
};

Offre.updateById = (offre_id, updatedOffre, callback) => {
  let updateFields = [];
  let query = 'UPDATE Offre SET ';

  if (updatedOffre.state) {
    query += 'state = ?, ';
    updateFields.push(updatedOffre.state);
  }

  if (updatedOffre.date_v_debut) {
    query += 'date_v_debut = ?, ';
    updateFields.push(updatedOffre.date_v_debut);
  }

  if (updatedOffre.date_v_fin) {
    query += 'date_v_fin = ?, ';
    updateFields.push(updatedOffre.date_v_fin);
  }
  // Remove the trailing comma and space from the query
  query = query.slice(0, -2);

  // Add the WHERE clause to specify the user_id
  query += ' WHERE offre_id = ?';

  // Add the user_id to the updateFields array
  updateFields.push(offre_id);

  db.query(query, updateFields, (error, results, fields) => {
    console.log(query);
    console.log(updatedOffre);
    if (error) {
      return callback(error, null);
    }
    return callback(null, results);
  });
};




module.exports = Offre;
