const DB = require('./models/db.js');
const mockExecute = jest.fn();
DB.execute = mockExecute;

const Utilisateur = require('./models/Utilisateur.js');
const Organisation = require('./models/Organisation.js');
const Offre = require('./models/Offre.js');
const FichePoste = require('./models/FichePoste.js');
const userModel = require('./models/Utilisateur.js');
const offreModel = require('./models/Offre.js');

const request = require('supertest');
const express = require('express');


const app = require("./app");





describe('Test de la route "/home"', () => {
    test('Redirige vers la page d\'accueil si l\'utilisateur n\'est pas connecté', done => {
      request(app)
        .get('/recruiter/home')
        .expect(302)
        .expect('Location', '/')
        .end(done);
    });
  
    test('Redirige vers la page d\'accueil si le type d\'utilisateur n\'est pas "recruteur"', done => {
      const fakeSession = { userId: 123, user_type: 'candidat' };
      request(app)
        .get('/recruiter/home')
        .query(fakeSession) // Simuler la session en tant que paramètres de requête
        .expect(302)
        .expect('Location', '/')
        .end(done);
    });
  
    test('Rend la page "RecruiterHomePage" avec les offres de l\'organisation si les conditions sont remplies', done => {
      const fakeSession = { userId: 123, user_type: 'recruteur', organisation: '12345679' };
      const fakeOffreResult = ['offre1', 'offre2'];
  
      offreModel.getByOrganisation = jest.fn((organisation, callback) => {
        callback(null, fakeOffreResult);
      });
  
      request(app)
        .get('/recruiter/home')
        .query(fakeSession) // Simuler la session en tant que paramètres de requête
        .expect(302)
        .end(done);
    });
  });

describe("Test the root path", () => {
    test("It should response the GET method", done => {
        request(app)
            .get("/")
            .then(response => {
                expect(response.statusCode).toBe(200);
                done();
            });
    });
});

describe('Test de la route "/createOffer"', () => {
    test('Redirige vers la page de connexion si l\'utilisateur n\'est pas connecté', done => {
      request(app)
        .get('/recruiter/createOffer')
        .expect(302)
        .expect('Location', '/')
        .end(done);
    });
  
    test('Redirige vers la page d\'accueil si le type d\'utilisateur n\'est pas "recruteur"', done => {
      const fakeSession = { userId: 123, user_type: 'candidat' };
      request(app)
        .get('/recruiter/createOffer')
        .query(fakeSession)
        .expect(302)
        .expect('Location', '/')
        .end(done);
    });
  
    test('Rend la page "CreateOffer" avec les détails de l\'utilisateur', done => {
      const fakeSession = { userId: 123, user_type: 'recruteur', organisation: 'nom_de_l_organisation' };
      const fakeUserResult = { userId: 123, name: 'John Doe' };
  
      userModel.getById = jest.fn((userId, callback) => {
        callback(null, fakeUserResult);
      });
  
      request(app)
        .get('/recruiter/createOffer')
        .query(fakeSession)
        .expect(302)
        .end(done);
    });
  });
  
  describe('Test de la route "/creatingOffer"', () => {
    test('Crée une offre avec les données fournies et redirige vers la page d\'accueil des recruteurs', done => {
      const fakeSession = { userId: 123, user_type: 'recruteur', organisation: 'nom_de_l_organisation' };
      const fakeRequestBody = {
        intitule: 'Offre d\'emploi',
        description: 'Description de l\'offre',
        type_metier: 'Type de métier',
        resp: 'Responsabilités de l\'offre',
        loc: 'Lieu de travail',
        horaire: 'Horaire de travail',
        salairemin: '50000',
        salairemax: '70000',
        day_off: '20',
        remote: '1',
      };
  
      offreModel.createWithFiche = jest.fn((offreData, fichePosteData, callback) => {
        callback(null, { success: true });
      });
  
      request(app)
        .post('/recruiter/creatingOffer')
        .query(fakeSession)
        .send(fakeRequestBody)
        .expect(302)
        .expect('Location', '/recruiter/home')
        .end(done);
    });
  
    test('Rend la page "CreateOffer" avec un message d\'erreur en cas d\'erreur lors de la création de l\'offre', done => {
      const fakeSession = { userId: 123, user_type: 'recruteur', organisation: 'nom_de_l_organisation' };
      const fakeRequestBody = {
        intitule: 'Offre d\'emploi',
        description: 'Description de l\'offre',
        type_metier: 'Type de métier',
        resp: 'Responsabilités de l\'offre',
        loc: 'Lieu de travail',
        horaire: 'Horaire de travail',
        salairemin: '50000',
        salairemax: '70000',
        day_off: '20',
        remote: '1',
      };
  
      const fakeError = new Error('Une erreur s\'est produite lors de la création de l\'offre');
  
      offreModel.createWithFiche = jest.fn((offreData, fichePosteData, callback) => {
        callback(fakeError, null);
      });
  
      request(app)
        .post('/recruiter/creatingOffer')
        .query(fakeSession)
        .send(fakeRequestBody)
        .expect(200)
        .expect('Content-Type', /html/)
        .end(done);
    });
  });
  
  
jest.mock('./models/db.js', () => {
    return {
        query: jest.fn(),
    };
});

describe('Utilisateur', () => {
    beforeEach(() => {
        // Ajoutez ici la logique de configuration de la base de données pour les tests
    });

    afterEach(() => {
        // Ajoutez ici la logique de nettoyage de la base de données après chaque test
    });

    test('create() devrait insérer un nouvel utilisateur dans la base de données', () => {
        const utilisateur = {
            mail: 'test@example.com',
            password: 'password',
            nom: 'John',
            prenom: 'Doe',
            phone: '1234567890',
            datecrea: '2023-06-21',
            statut: 'actif',
            user_type: 'candidat',
            organisation: '123'
        };

        Utilisateur.create(utilisateur, (error, result) => {
            expect(error).toBeNull();
            expect(result).toBeDefined();
            // Ajoutez ici les assertions supplémentaires pour vérifier l'insertion de l'utilisateur
        });
    });

    test('getById() devrait récupérer un utilisateur par ID depuis la base de données', () => {
        const userId = 42;

        Utilisateur.getById(userId, (error, result) => {
            expect(error).toBeNull();
            expect(result).toBeDefined();
            // Ajoutez ici les assertions supplémentaires pour vérifier les détails de l'utilisateur récupéré
        });
    });

    test('getByStatus() devrait récupérer les utilisateurs par statut depuis la base de données', () => {
        const status = 'actif';

        Utilisateur.getByStatus(status, (error, result) => {
            expect(error).toBeNull();
            expect(result).toBeDefined();
            // Ajoutez ici les assertions supplémentaires pour vérifier les utilisateurs récupérés
        });
    });

    test('getByType() devrait récupérer les utilisateurs par type depuis la base de données', () => {
        const type = 'recruteur';

        Utilisateur.getByType(type, (error, result) => {
            expect(error).toBeNull();
            expect(result).toBeDefined();
            // Ajoutez ici les assertions supplémentaires pour vérifier les utilisateurs récupérés
        });
    });

    test('getByStatusAndType() devrait récupérer les utilisateurs par statut et type depuis la base de données', () => {
        const status = 'inactif';
        const type = 'recruteur';

        Utilisateur.getByStatusAndType(status, type, (error, result) => {
            expect(error).toBeNull();
            expect(result).toBeDefined();
            // Ajoutez ici les assertions supplémentaires pour vérifier les utilisateurs récupérés
        });
    });

    test('getFirstRecruiters() devrait récupérer les premiers recruteurs inactifs sans recruteur actif associé à la même organisation', () => {
        Utilisateur.getFirstRecruiters((error, result) => {
            expect(error).toBeNull();
            expect(result).toBeDefined();
            // Ajoutez ici les assertions supplémentaires pour vérifier les recruteurs récupérés
        });
    });

    test('getByEmail() devrait récupérer un utilisateur par adresse e-mail depuis la base de données', () => {
        const email = 'test@example.com';

        Utilisateur.getByEmail(email, (error, result) => {
            expect(error).toBeNull();
            expect(result).toBeDefined();
            // Ajoutez ici les assertions supplémentaires pour vérifier les détails de l'utilisateur récupéré
        });
    });

    test('getAll() devrait récupérer tous les utilisateurs depuis la base de données', () => {
        Utilisateur.getAll((error, result) => {
            expect(error).toBeNull();
            expect(result).toBeDefined();
            // Ajoutez ici les assertions supplémentaires pour vérifier tous les utilisateurs récupérés
        });
    });

    test('getUserCount() devrait retourner le nombre total d utilisateurs correspondant aux critères de recherche', () => {
        const criteria = 'John';

        Utilisateur.getUserCount(criteria, (error, result) => {
            expect(error).toBeNull();
            expect(result).toBeDefined();
            // Ajoutez ici les assertions supplémentaires pour vérifier le nombre total d'utilisateurs
        });
    });

    test('searchUser() devrait récupérer les utilisateurs correspondant aux critères de recherche', () => {
        const criteria = 'Frodo';
        const resultsPerPage = 10;
        const offset = 0;

        Utilisateur.searchUser(criteria, resultsPerPage, offset, (error, result) => {
            expect(error).toBeNull();
            expect(result).toBeDefined();
            expect(result).toHaveBeenCalledWith(`
            SELECT *
            FROM Utilisateur
            WHERE LOWER(nom) LIKE CONCAT('%', LOWER(?), '%')
              OR LOWER(prenom) LIKE CONCAT('%', LOWER(?), '%')
            LIMIT ? OFFSET ?;
          `)
            // Ajoutez ici les assertions supplémentaires pour vérifier les utilisateurs récupérés
        });
    });

    test('updateById() devrait mettre à jour un utilisateur par ID dans la base de données', () => {
        const userId = 42;
        const utilisateur = {
            nom: 'NouveauNom',
            prenom: 'NouveauPrenom',
        };

        Utilisateur.updateById(userId, utilisateur, (error, result) => {
            expect(error).toBeNull();
            expect(result).toBeDefined();
            // Ajoutez ici les assertions supplémentaires pour vérifier la mise à jour de l'utilisateur
        });
    });

    test('deleteById() devrait supprimer un utilisateur par ID de la base de données', () => {
        const userId = 42;

        Utilisateur.deleteById(userId, (error, result) => {
            expect(error).toBeNull();
            expect(result).toBeDefined();
            // Ajoutez ici les assertions supplémentaires pour vérifier la suppression de l'utilisateur
        });
    });

    test('deleteByEmail() devrait supprimer un utilisateur par adresse e-mail de la base de données', () => {
        const email = 'test@example.com';

        Utilisateur.deleteByEmail(email, (error, result) => {
            expect(error).toBeNull();
            expect(result).toBeDefined();
            // Ajoutez ici les assertions supplémentaires pour vérifier la suppression de l'utilisateur
        });
    });

    test('authenticateUser() devrait authentifier un utilisateur par adresse e-mail et mot de passe', () => {
        const email = 'test@example.com';
        const password = 'password';

        Utilisateur.authenticateUser(email, password, (error, result) => {
            expect(error).toBeNull();
            expect(result).toBeDefined();
            // Ajoutez ici les assertions supplémentaires pour vérifier l'authentification de l'utilisateur
        });
    });

    test('getByOrgaRecru() devrait récupérer les recruteurs inactifs d une organisation donnée', () => {
        const organisation = '123';

        Utilisateur.getByOrgaRecru(organisation, (error, result) => {
            expect(error).toBeNull();
            expect(result).toBeDefined();
            // Ajoutez ici les assertions supplémentaires pour vérifier les recruteurs récupérés
        });
    });
    // Ajoutez d'autres tests pour les autres méthodes de l'objet Utilisateur
});

describe('Organisation', () => {
    test('create method should create a new organisation', () => {
        const newOrganisation = {
            siren: '123',
            name: 'test',
            org_type: 'test',
            head_office: 'test',
        };

        Organisation.create(newOrganisation, (error, results) => {
            expect(error).toBeNull();
            expect(results).toBeDefined();
            // Vérifiez d'autres assertions si nécessaire
        });
    });

    test('getById method should get an organisation by its siren', () => {
        const siren = '123'; // Remplacez par le siren de l'organisation souhaitée

        Organisation.getById(siren, (error, results) => {
            expect(error).toBeNull();
            expect(results).toBeDefined();
            // Vérifiez d'autres assertions si nécessaire

        });
    });

    test('getByNom method should get an organisation by its name', () => {
        const nom = 'test'; // Remplacez par le nom de l'organisation souhaitée

        Organisation.getByNom(nom, (error, results) => {
            expect(error).toBeNull();
            expect(results).toBeDefined();
            // Vérifiez d'autres assertions si nécessaire

        });
    });

    test('getByType method should get organisations by their type', () => {
        const org_type = 'test'; // Remplacez par le type d'organisation souhaité

        Organisation.getByType(org_type, (error, results) => {
            expect(error).toBeNull();
            expect(results).toBeDefined();
            // Vérifiez d'autres assertions si nécessaire

        });
    });

    test('getByLocation method should get organisations by their head office location', () => {
        const head_office = 'test'; // Remplacez par l'adresse du siège social souhaitée

        Organisation.getByLocation(head_office, (error, results) => {
            expect(error).toBeNull();
            expect(results).toBeDefined();
            // Vérifiez d'autres assertions si nécessaire

        });
    });

    test('getAll method should get all organisations', () => {
        Organisation.getAll((error, results) => {
            expect(error).toBeNull();
            expect(results).toBeDefined();
            // Vérifiez d'autres assertions si nécessaire

        });
    });

    test('searchOrganisation method should search for organisations based on criteria', () => {
        const criteria = null; // Remplacez par le critère de recherche souhaité
        const resultsPerPage = 10; // Remplacez par le nombre de résultats par page souhaité
        const offset = 0; // Remplacez par le décalage souhaité

        Organisation.searchOrganisation(criteria, resultsPerPage, offset, (error, results) => {
            expect(error).toBeNull();
            expect(results).toBeDefined();
            // Vérifiez d'autres assertions si nécessaire

        });
    });

    test('getOrganisationCount method should get the count of organisations based on criteria', () => {
        const criteria = null; // Remplacez par le critère de recherche souhaité

        Organisation.getOrganisationCount(criteria, (error, totalCount) => {
            expect(error).toBeNull();
            expect(totalCount).toBeDefined();
            // Vérifiez d'autres assertions si nécessaire

        });
    });

    test('updateById method should update an organisation by its siren', () => {
        const siren = '123'; // Remplacez par le siren de l'organisation à mettre à jour
        const updatedOrganisation = {
            // Données mises à jour pour l'organisation
        };

        Organisation.updateById(siren, updatedOrganisation, (error, results) => {
            expect(error).toBeNull();
            expect(results).toBeDefined();
            // Vérifiez d'autres assertions si nécessaire

        });
    });

    test('delete method should delete an organisation by its siren', () => {
        const siren = '123'; // Remplacez par le siren de l'organisation à supprimer

        Organisation.delete(siren, (error, results) => {
            expect(error).toBeNull();
            expect(results).toBeDefined();
            // Vérifiez d'autres assertions si nécessaire

        });
    });

    test('getOffresWithFilter method should retrieve offres with the specified filters', () => {
        const location = 'test';
        const minSalary = 200000;
        const maxSalary = 30000;
        const salary = 3000;

        const expectedQuery = `SELECT * FROM Offre o JOIN Fiche_poste f ON o.offre_id = f.offre_id WHERE 1=1 AND o.organisation IN (SELECT siren FROM Organisation WHERE lower(head_office) LIKE lower('%${location}%')) AND f.salairemin >= ${minSalary} AND f.salairemax <= ${maxSalary} AND (f.salairemin <= ${salary} OR f.salairemax >= ${salary})`;

        const callback = jest.fn();
        Offre.getOffresWithFilter(location, minSalary, maxSalary, salary, callback);

        expect(DB.query).toHaveBeenCalledWith(expectedQuery, expect.any(Function));
    });
});

describe('Offre Model', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    test('create method should insert a new offer in the database', () => {
        const mockCallback = jest.fn();
        const mockResult = { insertId: 1 };
        const offerData = {
            state: 'publiee',
            organisation: '123456789',
            recruteur: 'John Doe',
        };

        const expectedQuery = 'INSERT INTO Offre (state, organisation, recruteur) VALUES (?, ?, ?)';


        DB.query.mockImplementation((query, values, callback) => {
            expect(query).toBe(expectedQuery);
            expect(values).toEqual([offerData.state, offerData.organisation, offerData.recruteur]);
            callback(null, mockResult);
        });

        Offre.create(offerData.state, offerData.organisation, offerData.recruteur, mockCallback);

        expect(mockCallback).toHaveBeenCalledTimes(1);
        expect(mockCallback).toHaveBeenCalledWith(null, mockResult.insertId);
    });

    test('getAll method should retrieve all offers from the database', () => {
        const mockCallback = jest.fn();
        const mockResult = [{ offre_id: 1, state: 'publiee', organisation: '123456789' }];

        const expectedQuery =
            'SELECT * FROM Offre INNER JOIN Fiche_poste INNER JOIN Organisation WHERE Fiche_poste.offre_id=Offre.offre_id AND Organisation.siren=Offre.organisation   ';


        DB.query.mockImplementation((query, callback) => {
            expect(query).toBe(expectedQuery);
            callback(null, mockResult);
        });

        Offre.getAll(mockCallback);

        expect(mockCallback).toHaveBeenCalledTimes(1);
        expect(mockCallback).toHaveBeenCalledWith(null, mockResult);
    });

    test('getById method should retrieve a specific offer from the database', () => {
        const mockCallback = jest.fn();
        const mockResult = { offre_id: 1, state: 'publiee', organisation: '123456789' };
        const offerId = 1;

        const expectedQuery =
            'SELECT * FROM Offre INNER JOIN Fiche_poste INNER JOIN Organisation WHERE Fiche_poste.offre_id=Offre.offre_id AND Organisation.siren=Offre.organisation AND Offre.offre_id = ?';


        DB.query.mockImplementation((query, values, callback) => {
            expect(query).toBe(expectedQuery);
            callback(null, mockResult);
        });

        Offre.getById(offerId, mockCallback);

        expect(mockCallback).toHaveBeenCalledTimes(1);
        expect(mockCallback).toHaveBeenCalledWith(null, mockResult);
    });


    test('delete method should delete an offer from the database', () => {
        const mockCallback = jest.fn();
        const mockResult = { affectedRows: 1 };
        const offerId = 1;

        const expectedQuery = '   DELETE Offre, Fiche_poste FROM Offre LEFT JOIN Fiche_poste ON Offre.offre_id = Fiche_poste.offre_id WHERE Offre.offre_id = ?';


        DB.query.mockImplementation((query, values, callback) => {
            expect(values).toEqual([offerId]);
            callback(null, mockResult);
        });

        Offre.delete(offerId, mockCallback);

        expect(mockCallback).toHaveBeenCalledTimes(1);
        expect(mockCallback).toHaveBeenCalledWith(null, 1);
    });

    test('getByState method should retrieve offers by state from the database', () => {
        const mockCallback = jest.fn();
        const mockResult = [{ offre_id: 1, state: 'publiee', organisation: '123456789' }];
        const state = 'publiee';

        const expectedQuery =
            'SELECT * FROM Offre INNER JOIN Fiche_poste INNER JOIN Organisation WHERE Fiche_poste.offre_id=Offre.offre_id AND Organisation.siren=Offre.organisation AND Offre.state = ?  ';

        DB.query.mockImplementation((query, values, callback) => {
            expect(query).toBe(expectedQuery);
            expect(values).toEqual([state]);
            callback(null, mockResult);
        });

        Offre.getByState(state, mockCallback);

        expect(mockCallback).toHaveBeenCalledTimes(1);
        expect(mockCallback).toHaveBeenCalledWith(null, mockResult);
    });

    test('getByOrganisation method should retrieve offers by organisation from the database', () => {
        const mockCallback = jest.fn();
        const mockResult = [{ offre_id: 1, state: 'publiee', organisation: '123456789' }];
        const organisation = '123456789';

        const expectedQuery =
            'SELECT * FROM Offre INNER JOIN Fiche_poste INNER JOIN Organisation WHERE Fiche_poste.offre_id=Offre.offre_id AND Organisation.siren=Offre.organisation AND Offre.organisation = ?';


        DB.query.mockImplementation((query, values, callback) => {
            expect(query).toBe(expectedQuery);
            expect(values).toEqual([organisation]);
            callback(null, mockResult);
        });

        Offre.getByOrganisation(organisation, mockCallback);

        expect(mockCallback).toHaveBeenCalledTimes(1);
    });




});
