## Rapport de sécurité

### Vulnérabilité 1 : Contrôle d'accès insuffisant

Définition : Un utilisateur qui ne dispose pas du rôle "recruteur" peut déposer une offre s'il connaît l'URL associée à cette fonctionnalité, car les routes ne sont pas protégées par rôle.

1. Faiblesses détectées :

Les droits d'accès des utilisateurs ne sont pas correctement contrôlés pour les différentes actions de l'application. En conséquence, un utilisateur non autorisé peut accéder à des fonctionnalités réservées aux utilisateurs ayant le rôle "recruteur".

2. Méthodes de protection :

Pour remédier à cette vulnérabilité et garantir un contrôle d'accès adéquat, nous avons mis en place les mesures suivantes :

Implémentation d'un système de gestion des rôles et des autorisations : Chaque utilisateur se voit attribuer un rôle spécifique, et les fonctionnalités de l'application sont protégées en vérifiant les autorisations requises pour accéder à chaque action.

*Stratégie choisie* : Vulnérabilité considérée dès le début

*Correctif proposé :* Nous avons contrôlé les droits de l'utilisateur sur les différentes actions en utilisant un système de gestion des rôles et des autorisations. Ainsi, seuls les utilisateurs autorisés disposant du rôle "recruteur" peuvent accéder et utiliser la fonctionnalité de dépôt d'offres.

*Exemple de code :*
`router.get('/member', (req, res, next) => {
    const userId = req.session.userId;
    if (!userId) {
        return res.redirect('/');
    }
    if (req.session.user_type !== 'recruteur') {
        return res.redirect('/');
    }
    const Utilisateur = {
        nom: req.session.userName,
        prenom: req.session.userSurname,
    };
    res.render('Recruteur/Profil/BecomeMember', { title: 'BecomeMember', data: Utilisateur });
});`


### Vulnérabilité 2 : Injection SQL

Définition : L'injection SQL est une technique d'attaque permettant à un attaquant d'insérer du code SQL malveillant dans une requête, ce qui peut entraîner des failles de sécurité et la compromission de la base de données.

1. Faiblesses détectées :

L'application construisait les requêtes SQL en utilisant directement les paramètres fournis par l'utilisateur, sans validation ou échappement appropriés.

2. Méthodes de protection :

Pour remédier à cette faiblesse, nous avons utilisé des requêtes paramétrées et des procédures stockées pour éviter l'injection SQL.

Utilisation de requêtes paramétrées : Les paramètres de requête sont transmis séparément de la requête SQL, évitant ainsi toute injection de code.
Utilisation de procédures stockées : Les instructions SQL sont pré-définies dans des procédures stockées sécurisées, limitant ainsi les risques d'injection SQL.

*Stratégie choisie :* Vulnérabilité considérée dès le début

*Correctif proposé :* Nous avons implémenté des requêtes paramétrées pour la majorité des fonctionnalités, garantissant ainsi une protection contre l'injection SQL dès le début du développement de l'application.

*Exemple de code :*
`Utilisateur.getById = (id, callback) => {
  db.query('SELECT * FROM Utilisateur WHERE user_id = ?', [id], (error, results, fields) => {
    if (error) {
      return callback(error, null);
    }
    return callback(null, results[0]);
  });
};`

### Vulnérabilité 3 : Bruteforce

Définition : L'attaque par bruteforce consiste à essayer systématiquement toutes les combinaisons possibles de mots de passe jusqu'à ce que le mot de passe correct soit trouvé.

1. Faiblesses détectées :

L'application ne limitait pas le nombre de tentatives de connexion, ce qui rendait possible une attaque par bruteforce.

2. Méthodes de protection :

Pour renforcer la sécurité contre les attaques par bruteforce, nous avons mis en place les mesures suivantes :

Mise en place d'un mécanisme de blocage temporaire des tentatives de connexions après un certain nombre de tentatives infructueuses : Après un nombre défini de tentatives de connexion échouées, les tentatives de connexion sont bloqué temporairement pour décourager les attaques par bruteforce.
Utilisation de politiques de mots de passe solides : Nous avons encouragé les utilisateurs à choisir des mots de passe forts et à appliquer des règles de complexité pour renforcer la sécurité des comptes.(CNIL)

*Stratégie choisie :* Vulnérabilité détectée/considérée vers la fin

*Correctif proposé :* Nous avons implémenté un mécanisme de verrouillage des tentatives de connexion après 5 tentatives de connexion infructueuses, protégeant ainsi l'application contre les attaques par bruteforce.

*Extrait du code :*
    `const maxFailedAttempts = 5;
    const blockDuration = 30 * 1000; // 30 seconds

    const blockedUntil = req.session.blockedUntil;

    if (blockedUntil && blockedUntil > Date.now()) {
        const remainingTime = Math.ceil((blockedUntil - Date.now()) / 1000);
        return res.render('Connexion', { error: `Too many failed login attempts Please try again after ${remainingTime} sec.` });
    } else if (blockedUntil && blockedUntil < Date.now()) {
        req.session.failedAttempts = 0;
    }`


### Autres actions de sécurité

1. Chiffrement des mots de passe :

Avant d'être enregistrées dans la base de données, les données sensibles, comme les mots de passe, sont chiffrées à l'aide de bcrypt. Cela garantit que seules les personnes autorisées peuvent accéder aux informations.

2. Protection de session :

Nous avons aussi protégé les sessions et limité leurs temps d'existence grâce au code suivant :

`app.use(cookieParser());

const secret = crypto.randomBytes(32).toString('hex');
const deuxHeures = 1000 * 60 * 60 * 2;

app.use(session({
  secret: secret,
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false, maxAge: deuxHeures,httpOnly: true}
}));`


